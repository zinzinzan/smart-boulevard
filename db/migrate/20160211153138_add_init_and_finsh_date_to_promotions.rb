class AddInitAndFinshDateToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :initial_date, :datetime
    add_column :promotions, :finish_date, :datetime
  end
end
