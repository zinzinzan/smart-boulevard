class PlaceOfInterest < ActiveRecord::Base
  has_many :photos, dependent: :destroy
  has_one :event
  validates :name, :description, presence: true

  self.per_page = 5
end

