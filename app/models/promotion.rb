class Promotion < ActiveRecord::Base
  belongs_to :store
  has_many :reviews, dependent: :destroy
  default_scope -> { order(initial_date: :asc) }
  #validates :rate, numericality: {only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5}
  mount_uploader :image, PictureUploader
  scope :starts_with, -> (title) { where("lower(title) like ?", "#{title}%") }

  validates :title,:store, presence: true

  self.per_page = 5

end
