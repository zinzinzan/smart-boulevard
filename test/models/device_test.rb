require 'test_helper'

class DeviceTest < ActiveSupport::TestCase


  def setup
    @user = User.new(user_name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
    @user.save!
    @device = Device.new(user_id: @user.id, operative_system: ANDROID, token_device: 'qwerty')
  end

  #------------------------------------------------------------------------------------------

  test "user set save a valid device" do
    assert @device.valid?
  end

  #------------------------------------------------------------------------------------------

  test "user device should not be valid" do
    @device = Device.new(user_id: @user.id, operative_system: '', token_device: '')
    assert_not @device.valid?
  end

  #------------------------------------------------------------------------------------------

  test "edit should not be valid" do
    @device.update_attribute(:token_device, '')
    assert_not @device.valid?
  end


end

