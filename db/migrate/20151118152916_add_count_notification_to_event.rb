class AddCountNotificationToEvent < ActiveRecord::Migration
  def change
    add_column :events, :count_notification, :integer
  end
end
