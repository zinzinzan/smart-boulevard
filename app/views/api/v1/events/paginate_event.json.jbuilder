json.events @events do |event|

  json.id event.id
  json.image request.protocol + request.host_with_port + event.image.to_s

  begin
    json.projection event.projection
  rescue NoMethodError
    json.projection 'promo'
  end
#----------------------------------------------------------------------
  begin
    json.name event.name
  rescue NoMethodError
    json.name event.title
  end
#----------------------------------------------------------------------
  begin
    json.date event.date
  rescue NoMethodError
    json.date event.initial_date
  end
#----------------------------------------------------------------------
  begin
    json.final_date event.finish_date
  rescue NoMethodError
    json.final_date 'empty final date this is a event or project'
  end
#----------------------------------------------------------------------
  begin
    json.logo_sponsor request.protocol + request.host_with_port + event.logo_sponsor.to_s
  rescue NoMethodError
    json.logo_sponsor 'empty logo this is a promo'
  end
#----------------------------------------------------------------------
  begin
    json.comments_count event.comments.count;
  rescue NoMethodError
    json.comments_count 'empty comment this is a promo'
  end
#----------------------------------------------------------------------
  begin
    json.city event.city
  rescue NoMethodError
    json.city 'empty this is a promo'
  end
#----------------------------------------------------------------------
  begin
    json.price event.price
  rescue NoMethodError
    json.price 'empty'
  end
#----------------------------------------------------------------------
  begin
    json.promo_price event.promo_price
  rescue NoMethodError
    json.promo_price 'empty'
  end

end