json.events @events do |event|
  json.id event.id
  json.name event.name
  json.date event.date
  json.projection event.projection
  json.city event.city
  json.logo_sponsor request.protocol + request.host_with_port + event.logo_sponsor.url
  json.image request.protocol + request.host_with_port + event.image.to_s


  json.comments_count event.comments.count;
end