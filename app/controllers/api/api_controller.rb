class Api::ApiController < ActionController::Base

  before_filter :add_allow_credentials_headers

  #----------------------------------------------------------------------

  def add_allow_credentials_headers
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Access_control_CORS#section_5
    #
    # Because we want our front-end to send cookies to allow the API to be authenticated
    # (using 'withCredentials' in the XMLHttpRequest), we need to add some headers so
    # the browser will not reject the response
    response.headers['Access-Control-Allow-Origin'] = request.headers['Origin'] || '*'
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    # response.headers['Access-Control-Allow-Methods'] = 'POST,GET, PUT, PATCH, OPTIONS'
  end

  #----------------------------------------------------------------------

  def options
    head :status => 200, :'Access-Control-Allow-Headers' => 'accept, content-type, X-Api-Key'
    #head :status => 200, :'Access-Control-Allow-Methods' => 'POST,GET, PUT, PATCH, OPTIONS'
  end

  private

  #----------------------------------------------------------------------

  def authenticate
    api_key = request.headers['X-Api-Key']
    @user = User.where(api_key: api_key).first if api_key

    unless @user
      head status: :unauthorized
      return false
    end
  end

  #----------------------------------------------------------------------

  def check_user

    api_key = request.headers['X-Api-Key']
    @user = User.where(api_key: api_key).first if api_key

    unless @user
      render json: {success: 'login_or_register'}
      return
    end
  end

end