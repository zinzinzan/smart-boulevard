class Api::V1::PlaceOfInterestsController < Api::ApiController

  def index_depor
    @place_of_interests = PlaceOfInterest.where("kind = ?", DEPORTIVO)
  end


  def index_cult
    @place_of_interests = PlaceOfInterest.where("kind = ?", CULTURAL)

  end

  def index_enter
    @place_of_interests = PlaceOfInterest.where("kind = ?", ENTRETENIMIENTO)

  end

  def index_adic
    @place_of_interests = PlaceOfInterest.where("kind = ?", ADICIONALES)
  end

  def index
    @place_of_interests = PlaceOfInterest.all
  end

  def show
    @place_of_interest = PlaceOfInterest.find(params[:id])
  end

end
