class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :detail
      t.boolean :projection
      t.datetime :date
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :events, :users
    add_index :events, [:created_at]
  end
end
