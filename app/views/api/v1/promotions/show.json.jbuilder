json.id @promo.id
json.title @promo.title
json.price @promo.price
json.promo_price @promo.promo_price
json.conditions @promo.conditions
json.image request.protocol + request.host_with_port + @promo.image.to_s
json.rate @media_rate
json.store_name @promo.store.name
json.store_logo request.protocol + request.host_with_port + @promo.store.logo.to_s
