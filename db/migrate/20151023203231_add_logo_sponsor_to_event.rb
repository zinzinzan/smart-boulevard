class AddLogoSponsorToEvent < ActiveRecord::Migration
  def change
    add_column :events, :logo_sponsor, :string
  end
end
