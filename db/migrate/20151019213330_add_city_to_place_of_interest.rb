class AddCityToPlaceOfInterest < ActiveRecord::Migration
  def change
    add_column :place_of_interests, :city, :string
  end
end
