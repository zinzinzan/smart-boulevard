class AddPlaceOfInterestToEvents < ActiveRecord::Migration
  def change
    add_reference :events, :place_of_interest, index: true
    add_foreign_key :events, :place_of_interests
  end
end
