class SessionsController < ApplicationController

  layout 'login'


  def new
    if logged_in?
      #if log in
      redirect_to root_url
    end
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password]) && (user.role.role_name == ADMIN_USER ||
        user.role.role_name == WEB_USER)
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      #after log in

      redirect_to root_url
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end

  end

  def destroy
    log_out if logged_in?
    redirect_to root_url;
  end
end
