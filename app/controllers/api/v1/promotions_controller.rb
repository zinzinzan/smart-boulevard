class Api::V1::PromotionsController < Api::ApiController

  respond_to :json

  before_action :init

  #----------------------------------------------------------------------

  def init
    @rate_count = 0;
    @reviews_count = 0;
  end

  #----------------------------------------------------------------------

  def index
    @promotions = Promotion.all
  end

  #----------------------------------------------------------------------

  def show


    begin
      @promo = Promotion.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      # handle other ActiveRecord errors
      render json: {error: 'no se encontro esta promocion en BD'}
      return

    end

    @reviews = Review.where("promotion_id = :promotion_id", {promotion_id: @promo.id})

    if @reviews.first

      @reviews.each do |review|

        @rate_count = @rate_count + review.rate;
        @reviews_count += 1;

      end

      @media_rate = @rate_count/@reviews_count

      return
    else
      @media_rate = 'Se el primero en calificarla'
    end
  end


  #----------------------------------------------------------------------

  def vote
    @promo = Promotion.find(params[:id])
    rate = params[:rate]
    @promo.vote_count = rate
    #@promo.summatory += rate;
    if @promo.save
      render json: {sucess: '1'}
    else
      render json: {sucess: '0'}
    end
  end

  #----------------------------------------------------------------------

  def last
    if Promotion.first.nil?
      render json: {error: 'no existen  promociones en BD'}
      return;
    end
    @promotions = []
    promo = Promotion.first
    @promotions.append(promo)
    if Promotion.second
      @promotions.append(Promotion.second)
    end
  end

end