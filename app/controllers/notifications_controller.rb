class NotificationsController < ApplicationController
  require 'net/http'

  def subregion_options
    # config.i18n.default_locale = :en
    render partial: 'subregion_select'
  end

  def subsubregion_options

    render partial: 'subsubregion_select'
  end

  def new
    if params[:event_id].present?
      @event = Event.find(params[:event_id])
    end

    if params[:promo_id].present?
      @promo = Promotion.find(params[:promo_id])

    end


  end


  def create
    title_param = params[:title]
    message_param = params[:body]
    country = Carmen::Country.coded(params[:country_code])
    if params[:order].present?
      city = country.subregions.coded(params[:order][:state_code]);
    end
    if params[:canton].present?
      canton = Canton.find(params[:canton])

    end

    gender = params[:gender]
    all_devices = []

    filter_age = params[:filter_age]
    filter_country = params[:filter_country]
    filter_gender = params[:filter_gender]
    filter_province = params[:filter_province]
    filter_canton = params[:filter_canton]

    notification_android = Rpush::Gcm::Notification.new
    notification_android.registration_ids = []
    notification_android.app = Rpush::Gcm::App.find_by_name("android_app")

    if params[:since].present? && params[:until].present?
      # se esta calculando la edad de el primer parametro que va desde enero hasta el mes con el dia actual
      today_year = Time.now.year
      year_from_param = today_year - params[:since].to_i
      date_january_from_param = Time.now.change(year: year_from_param, month: 1, day: 1)
      date_from_param = Time.now.change(year: year_from_param)

      # se calcula la edad del segundo parametro que va desde enero hasta el mes con el dia actual
      year_until_param = today_year - params[:until].to_i
      date_january_until = Time.now.change(year: year_until_param, month: 1, day: 1)
      date_until_param = Time.now.change(year: year_from_param)
    end


    if filter_country.nil? && filter_age.nil? && filter_gender.nil?

      # Enviar a todos los usuarios
      role_app_id = 2
      @users = User.where('users.role_id = :role_app_id',
                          role_app_id: role_app_id)

      #usando filtros
    else


      # filtramos por pais y genero

      if not filter_country.nil?


        if filter_gender != nil
          #por provincia y genero se puede obtimizar cambiardo el orden!!
          if not filter_province.nil?
            @users = User.where('users.province = :province_param AND users.gender = :gender_param',
                                province_param: city.to_s, gender_param: params[:gender])
            #canton y genero
            if not filter_canton.nil?
              @users = User.where('users.city = :city_param AND users.gender = :gender_param',
                                  city_param: canton.name, gender_param: params[:gender])
            end
          else
            @users = User.where('users.country = :country_param AND users.gender = :gender_param',
                                country_param: country.to_s, gender_param: params[:gender])
          end
        else
#only place
          if not filter_province.nil?
            @users = User.where('users.province = :province_param',
                                province_param: city.to_s)
            #canton y genero
            if not filter_canton.nil?
              @users = User.where('users.city = :city_param',
                                  city_param: canton.name)
            end
          else
            @users = User.where('users.country = :country_param',
                                country_param: country.to_s)
          end

        end

        #filtramos por pais y edad
        if filter_age != nil

          if not filter_province.nil?
            @users = User.where('users.country = :country_param AND users.province = :province_param AND
                               ((born_date >= :date_january_param AND born_date <= :date_from_param) OR
                               (born_date >= :date_january_until AND born_date <= :date_until_param))',
                                country_param: country.to_s, province_param: city.to_s,
                                date_january_param: date_january_from_param, date_from_param: date_from_param,
                                date_january_until: date_january_until, date_until_param: date_until_param)

            if not filter_canton.nil?

              @users = User.where('users.country = :country_param AND users.city = :city_param AND
                                  users.province = :province_param AND ((born_date >= :date_january_param AND
                                  born_date <= :date_from_param) OR (born_date >= :date_january_until AND
                                  born_date <= :date_until_param))', country_param: country.to_s,
                                  city_param: canton.name, province_param: canton.province,
                                  date_january_param: date_january_from_param, date_from_param: date_from_param,
                                  date_january_until: date_january_until, date_until_param: date_until_param)
            end
          else #filtramos por pais nada mas
            @users = User.where('users.country = :country_param AND ((born_date >= :date_january_param AND
                                born_date <= :date_from_param)    OR (born_date >= :date_january_until AND
                                born_date <= :date_until_param))', country_param: country.to_s,
                                date_january_param: date_january_from_param, date_from_param: date_from_param,
                                date_january_until: date_january_until, date_until_param: date_until_param)
          end


        end
        #todos los filtros (pais,genero y edad)


        if filter_gender != nil && filter_age != nil
          if not filter_province.nil?
            @users = User.where('users.country = :country_param AND users.province = :province_param AND
                                users.gender = :gender_param AND ((born_date >= :date_january_param AND
                                born_date <= :date_from_param) OR (born_date >= :date_january_until AND
                                born_date <= :date_until_param))',
                                country_param: country.to_s, province_param: city.to_s,
                                gender_param: params[:gender], date_january_param: date_january_from_param,
                                date_from_param: date_from_param, date_january_until: date_january_until,
                                date_until_param: date_until_param)

            if not filter_canton.nil?
              @users = User.where('users.country = :country_param AND users.city = :city_param AND
                                  users.province = :province_param AND users.gender = :gender_param AND
                                  ((born_date >= :date_january_param AND born_date <= :date_from_param) OR
                                  (born_date >= :date_january_until AND born_date <= :date_until_param))',
                                  country_param: country.to_s, city_param: canton.name,
                                  province_param: canton.province, gender_param: params[:gender],
                                  date_january_param: date_january_from_param, date_from_param: date_from_param,
                                  date_january_until: date_january_until, date_until_param: date_until_param)
            end
          else #filtra  pais, genero y edad
            @users = User.where('users.country = :country_param AND users.gender = :gender_param AND
                                ((born_date >= :date_january_param AND born_date <= :date_from_param) OR
                                (born_date >= :date_january_until AND born_date <= :date_until_param))',
                                country_param: country.to_s, gender_param: params[:gender],
                                date_january_param: date_january_from_param, date_from_param: date_from_param,
                                date_january_until: date_january_until, date_until_param: date_until_param)


          end
          ###############################################################
          #todos los filtros
          ###############################################################
        end
        #end pais
      end

      if  not filter_gender.nil?
        # filtramos por genero nada mas
        if filter_age == nil && filter_country == nil
          @users = User.where(gender: gender)
        end
        #filtramos por genero y edad
        if filter_age != nil
          @users = User.where('users.gender = :gender_param AND ((born_date >= :date_january_param AND born_date <= :date_from_param)
        OR (born_date >= :date_january_until AND born_date <= :date_until_param))', gender_param: params[:gender], date_january_param: date_january_from_param,
                              date_from_param: date_from_param, date_january_until: date_january_until, date_until_param: date_until_param)
        end

      end


      if  not filter_age.nil?
        ## filtramos por edad nada mas
        if filter_country == nil && filter_gender == nil
          @users = User.where('(born_date >= :date_january_param AND born_date <= :date_from_param)
        OR (born_date >= :date_january_until AND born_date <= :date_until_param)', date_january_param: date_january_from_param,
                              date_from_param: date_from_param, date_january_until: date_january_until, date_until_param: date_until_param)
        end

      end
      ###############################################################
      #se llena la lista de notificaciones dependiendo del filro
      ###############################################################


=begin
      @users.each do |user|

        if user.device != nil
          #puts "FILTRO/sssssss :  #{user.email}"
          if user.device.operative_system == ANDROID
            all_devices.push(user.device.token_device)
            notification_android.registration_ids.push(user.device.token_device)
          elsif user.device.operative_system == IOS
            notification_apple = Rpush::Apns::Notification.new
            notification_apple.app = Rpush::Apns::App.find_by_name("ios_app")
            notification_apple.device_token = user.device.token_device
            notification_apple.alert = title_param
            notification_apple.data = {title: title_param, message: message_param}
            notification_apple.save!
          end
        end

      end
=end

      #end del usando filtros
    end

    #ya paso por todos los querys
    @users.each do |user|
      if user.device != nil
        puts "-----------user------------ :  #{user.email}"
        if user.device.operative_system == ANDROID

          all_devices.push(user.device.token_device)
          notification_android.registration_ids.push(user.device.token_device)
          notification_android.app = Rpush::Gcm::App.find_by_name("android_app")
        elsif user.device.operative_system == IOS
          notification_apple = Rpush::Apns::Notification.new
          notification_apple.app = Rpush::Apns::App.find_by_name("ios_app")
          notification_apple.device_token = user.device.token_device
          notification_apple.alert = title_param
          notification_apple.data = {title: title_param, message: message_param}
          notification_apple.save!
        end
      end
    end


    if notification_android.registration_ids.length > 0
      notification_android.data = {title: title_param, message: message_param}
    end


    begin

      if current_user.web_user

        @user = current_user
        if @user.count_notification == @user.notifications_number
          flash[:danger] = "Supero el numero de notificaciones permitidas"
          redirect_to root_path
          return
        end

        @user.count_notification = @user.count_notification + 1
        @user.save
      end
      if notification_android.registration_ids.length > 0
        notification_android.save!
      end

      if params[:event_id].present?
        @event = Event.find(params[:event_id])
        @event.count_notification = @event.count_notification + 1
        @event.save
      end
      if params[:promo_id].present?
        @promo = Promotion.find(params[:promo_id])
        @promo.count_notification = @promo.count_notification + 1
        @promo.save
      end


    rescue ActiveRecord::RecordNotFound
      flash[:danger] = "No se pudo enviar la notificacion, ya que encontro ninguna coincidencia "

    rescue ActiveRecord::RecordInvalid
      flash[:danger] = "No se pudo enviar la notificacion, ya que encontro ninguna coincidencia "

    end
    flash[:success] = "Notificaciones enviadas correctamente"
    redirect_to :back

  end


  private

  def notification_params
    params.permit(:title, :body, :male, :female, :GLTBI, :since, :until,
                  :country_code, :canton, :filter_country, :filter_age, :filter_gender)
  end


end
