class AddBornDateToUser < ActiveRecord::Migration
  def change
    add_column :users, :born_date, :date,:null => true
  end
end
