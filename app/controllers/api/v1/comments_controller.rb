class Api::V1::CommentsController < Api::ApiController


  before_action :check_user, only: [:create]

  def create
    @comment = @user.comments.build(permit_params);
    if (@comment.save)
      render json: {
          body: @comment.body,
          author: User.find(@comment.user_id).user_name,
          created_at: 'menos de 1 minuto',
          created: Time.now
      }
    else
      render json: {sucess: '0'}
    end
  end

  #----------------------------------------------------------------------


  def index_for_event
    current_page = Integer(params[:index], 10)
    per_page = 10
    records_fetch_point = (current_page - 1) * per_page
    @comments = Comment.where('event_id = ?', (params[:event_id])).limit(per_page).offset(records_fetch_point)

  end

  #----------------------------------------------------------------------

  def permit_params
    params.permit(:event_id, :body)
  end



end