class AddNotificationBodyToPromotion < ActiveRecord::Migration
  def change
    add_column :promotions, :notification_body, :string
  end
end
