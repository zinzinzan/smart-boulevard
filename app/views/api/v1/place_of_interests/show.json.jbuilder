json.id @place_of_interest.id
json.name @place_of_interest.name
json.latitude @place_of_interest.latitude
json.longitude @place_of_interest.longitude
json.description @place_of_interest.description
json.photos @place_of_interest.photos do |photo|
  json.photo request.protocol + request.host_with_port + photo.photo_path.to_s
end