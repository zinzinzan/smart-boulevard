class AddSponsorToEvent < ActiveRecord::Migration
  def change
    add_column :events, :sponsor, :string
  end
end
