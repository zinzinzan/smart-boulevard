class AddConnectedToUser < ActiveRecord::Migration
  def change
    add_column :users, :connected, :boolean
  end
end
