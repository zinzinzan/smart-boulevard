class AddCountryToStore < ActiveRecord::Migration
  def change
    add_column :stores, :country, :string
  end
end
