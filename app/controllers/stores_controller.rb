class StoresController < ApplicationController

  include CanCan::ControllerAdditions

  load_and_authorize_resource

  def index
    if params[:per_page].present?
      Store.per_page = params[:per_page]
    end

    @stores = Store.where('stores.kind = :kind_store', kind_store: params[:kind]).
        paginate(page: params[:page])
    $kind_of_store = params[:kind]
  end

  def new
    @store = Store.new
    @photos = []
    for i in 0..5
      @photo = Photo.new
      @photos.append(@photo)
    end
  end

  def show
    @store = Store.find(params[:id])
  end

  def create

    unless params[:store_photo_0].present?
      flash[:danger] = 'Dedes cargar almenos una foto para el local'
      redirect_to :back
      return
    end

    @store = Store.new(store_params)
    @store.kind = params[:kind]
    if @store.save

      if params[:logo].present?
        @store.logo = params[:logo]
        @store.save
      end
      if params[:store_photo_0].present?
        @image0 = @store.photos.create(photo_path: params[:store_photo_0], store_id: @store.id)
        @image0.save
      end

      if params[:store_photo_1].present?
        @image1 = @store.photos.create(photo_path: params[:store_photo_1], store_id: @store.id)
        @image1.save
      end
      if params[:store_photo_2].present?
        @image2 = @store.photos.create(photo_path: params[:store_photo_2], store_id: @store.id)
        @image2.save
      end

      if params[:store_photo_3].present?
        @image3 = @store.photos.create(photo_path: params[:store_photo_3], store_id: @store.id)
        @image3.save
      end

      if params[:store_photo_4].present?
        @image4 = @store.photos.create(photo_path: params[:store_photo_4], store_id: @store.id)
        @image4.save
      end

      if params[:store_photo_5].present?
        @image5 = @store.photos.create(photo_path: params[:store_photo_5], store_id: @store.id)
        @image5.save
      end
      if params[:kind] == RESTAURANT
        redirect_to stores_path(kind: RESTAURANT)
      else
        redirect_to stores_path(kind: KIOSCO)
      end

    else
      redirect_to new
    end
  end

  def edit
    @store = Store.find(params[:id])
    @image_edit = @store.photos
  end

  def update
    @store = Store.find(params[:id])

    if @store.update_attributes(store_params)

      if params[:logo].present?
        @store.logo = params[:logo]
        @store.save
      end

      if params[:store_photo_0].present?
        @image0 = @store.photos[0]
        if @image0 == nil
          @image0 = @store.photos.create(photo_path: params[:store_photo_0], store_id: @store.id)
          @image0.save
        else
          @image0.photo_path = params[:store_photo_0]
          @image0.save
        end


      end

      if params[:store_photo_1].present?
        @image1 = @store.photos[1]
        if @image1 == nil
          @image1 = @store.photos.create(photo_path: params[:store_photo_1], store_id: @store.id)
          @image1.save
        else
          @image1.photo_path = params[:store_photo_1]
          @image1.save
        end

      end

      if params[:store_photo_2].present?
        @image2 = @store.photos[2]
        if @image2 == nil
          @image2 = @store.photos.create(photo_path: params[:store_photo_2], store_id: @store.id)
          @image2.save
        else
          @image2.photo_path = params[:store_photo_2]
          @image2.save
        end


      end

      if params[:store_photo_3].present?
        @image3 = @store.photos[3]
        if @image3 == nil
          @image3 = @store.photos.create(photo_path: params[:store_photo_3], store_id: @store.id)
          @image3.save
        else
          @image3.photo_path = params[:store_photo_3]
          @image3.save

        end


      end

      if params[:store_photo_4].present?
        @image4 = @store.photos[4]
        if @image4 == nil
          @image4 = @store.photos.create(photo_path: params[:store_photo_4], store_id: @store.id)
          @image4.save
        else
          @image4.photo_path = params[:store_photo_4]
          @image4.save
        end


      end

      if params[:store_photo_5].present?
        @image5 = @store.photos[5]
        if @image5 == nil
          @image5 = @store.photos.create(photo_path: params[:store_photo_3], store_id: @store.id)
          @image5.save
        else
          @image5.photo_path = params[:store_photo_5]
          @image5.save
        end


      end

      if @store.kind == RESTAURANT
        redirect_to stores_path(kind: RESTAURANT)
      else
        redirect_to stores_path(kind: KIOSCO)
      end
    else
      redirect_to edit_store_path

    end

  end

  def delete
    @store = Store.find(params[:id])
  end

  def destroy
    @store = Store.find(params[:id])
    @store.destroy
    @stores = Store.where('stores.kind = :kind_store', kind_store: $kind_of_store).paginate(page: params[:page],
                                                                                            per_page: 4)
  end


  def filter
    @stores = Store.where("lower(name) like (?) AND kind = (?)",
                          "%"+params[:store_to_search]+"%", params[:kind_of_store]).all
  end

  private

  def store_params
    params.require(:store).permit(:name, :description, :store_photo_0, :store_photo_1,
                                  :store_photo_2, :store_photo_3, :store_photo_4, :store_photo_5,
                                  :latitude, :longitude, :city, :country)

  end


end
