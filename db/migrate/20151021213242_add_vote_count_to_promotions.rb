class AddVoteCountToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :vote_count, :integer
  end
end
