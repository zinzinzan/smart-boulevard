class EventsController < ApplicationController

  include CanCan::ControllerAdditions
  load_and_authorize_resource

  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :create]
  # before_action :admin_user, only: [:destroy]

  def show
    @event = Event.find(params[:id])
  end

#------------------------------------------------------------------

  def new
    @event = Event.new
  end

#------------------------------------------------------------------
  def create
    @event = current_user.events.new(event_params)
    if params[:event_date].present?
      event_date = params[:event_date]
      time_date = params[:event_time]
      date_string = event_date + " " + time_date +" "+ "-0430"
      event_datetime = DateTime.strptime(date_string, '%m/%d/%Y %H:%M:%S %P %:z')
      @event.date = event_datetime
    end

    if params[:place_of_interest_id].present?
      @event.place_of_interest_id = params[:place_of_interest_id];
      @event.save
    end

    @event.count_notification = 0

    if @event.save
      if params[:image].present?
        @event.image = params[:image]
        @event.save
      end

      if params[:logo_sponsor].present?
        @event.logo_sponsor = params[:logo_sponsor]
        @event.save
      end
      flash[:success] = "Evento Creado"
      redirect_to :action => 'index'
    else
      render 'new';
    end

#------------------------------------------------------------------

  end

  def edit
    @event = Event.find(params[:id])
    if @event.logo_sponsor.url.nil?
      @event.logo_sponsor = '#'
    end
  end

#------------------------------------------------------------------

  def update
    @event = Event.find(params[:id])
    if params[:image].present?
      @event.image = params[:image]
      @event.save
    end

    if params[:event_date].present?
      event_date = params[:event_date]
      time_date = params[:event_time]
      date_string = event_date + " " + time_date +" "+ "-0430"
      event_datetime = DateTime.strptime(date_string, '%m/%d/%Y %H:%M:%S %P %:z')
      @event.date = event_datetime
    end

    if params[:place_of_interest_id].present?
      @event.place_of_interest_id = params[:place_of_interest_id];
      @event.save
    end

    if params[:logo_sponsor].present?
      @event.logo_sponsor = params[:logo_sponsor]
      @event.save
    end

    if @event.update_attributes(event_params)

      if params[:event][:projection] == '1'
        @event.sponsor = ""
        @event.remove_logo_sponsor!
        @event.place_of_interest_id =
            @event.save
      end

      flash[:success] = "Evento actualizado";
      redirect_to :action => 'index'
    else
      render 'edit'
    end
  end

#------------------------------------------------------------------

  def destroy
    Event.find(params[:id]).destroy
    @events = Event.paginate(page: params[:page])
  end

  def index
    if params[:per_page].present?
      Event.per_page = params[:per_page]
    end

    @events = Event.paginate(page: params[:page])
  end


  def filter
    @events = Event.where("lower(name) like (?)", "%" + params[:event_to_search]+"%")
  end

#------------------------------------------------------------------

  private

  def event_params
    params.require(:event).permit(:name, :detail, :projection, :sponsor,
                                  :notification_body, :latitude, :longitude,
                                  :duration, :city, :country, :place_of_interest_id)
  end

end
