class AddNotificationsNumberToUser < ActiveRecord::Migration
  def change
    add_column :users, :notifications_number, :integer
  end
end
