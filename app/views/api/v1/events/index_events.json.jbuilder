json.events @events do |event|
  json.id event.id
  json.name event.name
  json.date event.date
  json.time event.date.to_s(:time)
  json.projection event.projection
  json.image request.protocol + request.host_with_port + event.image.to_s
  json.logo_sponsor request.protocol + request.host_with_port + event.logo_sponsor.to_s
end