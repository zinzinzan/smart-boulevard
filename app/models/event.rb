class Event < ActiveRecord::Base
  belongs_to :user
  belongs_to :place_of_interest
  has_many :comments, dependent: :destroy
  default_scope -> { order(date: :asc) }
  validates :name, presence: true
  validates :detail, presence: true
  validates :date, presence: true
  mount_uploader :image, PictureUploader
  mount_uploader :logo_sponsor, PictureUploader

  self.per_page = 5


end
