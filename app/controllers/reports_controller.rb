class ReportsController < ApplicationController

  def show

  end

  #------------------------------------------------------------

  def filter
    $connected_users = false
    if params[:report_id] == '1'
      $start_date = params[:start_date_field]
      $end_date = params[:end_date_field] + ' ' + '23:59:59';
      @users = User.select("*").joins("INNER JOIN Roles ON users.role_id = roles.id").where("users.created_at >= :initial_date AND users.created_at <= :final_date AND roles.role_name = :role_name",
                                                                                            {initial_date: $start_date, final_date: $end_date, role_name: 'App_user'})
    else
      $connected_users = true
      @users = User.where('users.connected = :connected', connected: true)
    end

  end

  #------------------------------------------------------------

  def export_to_excel_users
    if $connected_users == false
      @users = User.joins("INNER JOIN Roles ON users.role_id = roles.id").where("users.created_at >= :initial_date AND users.created_at <= :final_date AND roles.role_name = :role_name",
                                                                                {initial_date: $start_date, final_date: $end_date, role_name: APP_USER})
    else
      # @users = User.joins("INNER JOIN Roles ON users.id = roles.id").where('users.connected = :connected AND roles.name = :role_name', connected: true,role_name: 'App_user')
      @users = User.where('users.connected = :connected', connected: true)
    end

    respond_to do |format|
      format.xlsx
    end
  end

  #------------------------------------------------------------

  private

  def filter_params
    params.permit(:start_date_field, :end_date_field, :report_id)
  end

end
