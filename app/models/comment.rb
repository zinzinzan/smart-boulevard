class Comment < ActiveRecord::Base
  belongs_to :event
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :event_id, presence: true
  validates :body, presence: true, length: {maximum: 512}
end