class AddCountNotificationToPromotion < ActiveRecord::Migration
  def change
    add_column :promotions, :count_notification, :integer
  end
end
