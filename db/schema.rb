# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160611032510) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cantons", force: :cascade do |t|
    t.string   "name"
    t.string   "province"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "comments", force: :cascade do |t|
    t.string   "body"
    t.integer  "event_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "comments", ["event_id"], name: "index_comments_on_event_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "connected_users", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "connected_users", ["user_id"], name: "index_connected_users_on_user_id", using: :btree

  create_table "devices", force: :cascade do |t|
    t.string   "token_device"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "operative_system"
  end

  add_index "devices", ["user_id"], name: "index_devices_on_user_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "name"
    t.string   "detail"
    t.boolean  "projection"
    t.datetime "date"
    t.integer  "user_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
    t.string   "country"
    t.string   "sponsor"
    t.string   "image"
    t.float    "duration"
    t.string   "logo_sponsor"
    t.string   "notification_body"
    t.integer  "count_notification"
    t.integer  "place_of_interest_id"
  end

  add_index "events", ["created_at"], name: "index_events_on_created_at", using: :btree
  add_index "events", ["place_of_interest_id"], name: "index_events_on_place_of_interest_id", using: :btree
  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.string   "photo_path"
    t.integer  "store_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "place_of_interest_id"
  end

  add_index "photos", ["place_of_interest_id"], name: "index_photos_on_place_of_interest_id", using: :btree
  add_index "photos", ["store_id"], name: "index_photos_on_store_id", using: :btree

  create_table "place_of_interests", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
    t.string   "country"
    t.string   "kind"
  end

  create_table "promotions", force: :cascade do |t|
    t.integer  "rate"
    t.string   "title"
    t.string   "image"
    t.float    "price"
    t.float    "promo_price"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "store_id"
    t.string   "conditions"
    t.integer  "vote_count"
    t.integer  "rate_summatory"
    t.string   "notification_body"
    t.integer  "count_notification"
    t.datetime "initial_date"
    t.datetime "finish_date"
  end

  add_index "promotions", ["created_at"], name: "index_promotions_on_created_at", using: :btree
  add_index "promotions", ["store_id"], name: "index_promotions_on_store_id", using: :btree

  create_table "reviews", force: :cascade do |t|
    t.integer  "rate"
    t.integer  "user_id"
    t.integer  "promotion_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "reviews", ["promotion_id"], name: "index_reviews_on_promotion_id", using: :btree
  add_index "reviews", ["user_id"], name: "index_reviews_on_user_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "role_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rpush_apps", force: :cascade do |t|
    t.string   "name",                                null: false
    t.string   "environment"
    t.text     "certificate"
    t.string   "password"
    t.integer  "connections",             default: 1, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type",                                null: false
    t.string   "auth_key"
    t.string   "client_id"
    t.string   "client_secret"
    t.string   "access_token"
    t.datetime "access_token_expiration"
  end

  create_table "rpush_feedback", force: :cascade do |t|
    t.string   "device_token", limit: 64, null: false
    t.datetime "failed_at",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "app_id"
  end

  add_index "rpush_feedback", ["device_token"], name: "index_rpush_feedback_on_device_token", using: :btree

  create_table "rpush_notifications", force: :cascade do |t|
    t.integer  "badge"
    t.string   "device_token",      limit: 64
    t.string   "sound",                        default: "default"
    t.string   "alert"
    t.text     "data"
    t.integer  "expiry",                       default: 86400
    t.boolean  "delivered",                    default: false,     null: false
    t.datetime "delivered_at"
    t.boolean  "failed",                       default: false,     null: false
    t.datetime "failed_at"
    t.integer  "error_code"
    t.text     "error_description"
    t.datetime "deliver_after"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "alert_is_json",                default: false
    t.string   "type",                                             null: false
    t.string   "collapse_key"
    t.boolean  "delay_while_idle",             default: false,     null: false
    t.text     "registration_ids"
    t.integer  "app_id",                                           null: false
    t.integer  "retries",                      default: 0
    t.string   "uri"
    t.datetime "fail_after"
    t.boolean  "processing",                   default: false,     null: false
    t.integer  "priority"
    t.text     "url_args"
    t.string   "category"
  end

  add_index "rpush_notifications", ["delivered", "failed"], name: "index_rpush_notifications_multi", where: "((NOT delivered) AND (NOT failed))", using: :btree

  create_table "stores", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "logo"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "city"
    t.string   "country"
    t.string   "kind"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "api_key"
    t.integer  "role_id"
    t.integer  "store_id"
    t.boolean  "connected"
    t.string   "city"
    t.string   "country"
    t.string   "province"
    t.date     "born_date"
    t.string   "last_name"
    t.boolean  "first_time"
    t.string   "gender"
    t.integer  "notifications_number"
    t.integer  "count_notification"
    t.string   "user_name"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
  end

  add_index "users", ["role_id"], name: "index_users_on_role_id", using: :btree
  add_index "users", ["store_id"], name: "index_users_on_store_id", using: :btree

  add_foreign_key "comments", "events"
  add_foreign_key "comments", "users"
  add_foreign_key "connected_users", "users"
  add_foreign_key "events", "place_of_interests"
  add_foreign_key "events", "users"
  add_foreign_key "photos", "place_of_interests"
  add_foreign_key "photos", "stores"
  add_foreign_key "promotions", "stores"
  add_foreign_key "reviews", "promotions"
  add_foreign_key "reviews", "users"
  add_foreign_key "users", "roles"
  add_foreign_key "users", "stores"
end
