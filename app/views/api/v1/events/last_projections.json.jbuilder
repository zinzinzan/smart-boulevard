json.projections @projections do |projection|
  json.id projection.id
  json.name projection.name
  json.date projection.date
  json.city projection.city
  #json.logo_sponsor request.protocol + request.host_with_port + projection.logo_sponsor.to_s
  json.image request.protocol + request.host_with_port + projection.image.to_s
end

