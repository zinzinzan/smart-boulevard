json.stores @stores do |store|
  json.id store.id
  json.description store.description
  json.latitude store.latitude
  json.longitude store.longitude
  json.name store.name
  json.logo request.protocol + request.host_with_port + store.logo.to_s
end