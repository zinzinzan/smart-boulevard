class User < ActiveRecord::Base
  #user sabe a quien pertenece
  belongs_to :store
  has_many :events, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :reviews, dependent: :destroy
  #device sabe quien es su dueño
  has_one :device, dependent: :destroy
  belongs_to :role
  attr_accessor :remember_token , :reset_token

  before_save { self.email = email.downcase }

  validates :user_name, presence: true, length: {maximum: 50}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates(:email, presence: true, length: {maximum: 255}, format: {with: VALID_EMAIL_REGEX},
            uniqueness: {case_sensitive: false})
  has_secure_password
  validates(:password, length: {minimum: 6}, allow_blank: true)


  accepts_nested_attributes_for :role, update_only: true

  self.per_page = 5

  before_create do |doc|
    doc.api_key = doc.generate_api_key
  end

  #--------------------------------------------------------------------------------

  def web_user
    self.role.role_name == WEB_USER
  end

  #--------------------------------------------------------------------------------

  def admin
    self.role.role_name == ADMIN_USER;
  end

  #--------------------------------------------------------------------------------

  def social_user
    self.role.role_name == SOCIAL_NETWORK_USER;
  end

  #--------------------------------------------------------------------------------

  def app_user
    self.role.role_name == APP_USER;
  end

  #--------------------------------------------------------------------------------

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :

        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  #------------------------------------------------------------

  def User.new_token
    SecureRandom.urlsafe_base64;
  end

  #------------------------------------------------------------

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(self.remember_token))
  end

  #------------------------------------------------------------

  def authenticated?( token, attribute = :remember)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end
  #------------------------------------------------------------

  def forget
    update_attribute(:remember_digest, nil)
  end


  def update_notification_count
    connection = ActiveRecord::Base.connection
    connection.execute("UPDATE users SET users.count_notification = 0;")
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end
  #------------------------------------------------------------


  #------------------------------------------------------------

  def generate_api_key
    loop do
      token = SecureRandom.base64.tr('+/=', 'Qrt')
      break token unless User.exists?(api_key: token)
    end
  end


end
