class CreatePlaceOfInterests < ActiveRecord::Migration
  def change
    create_table :place_of_interests do |t|
      t.string :name
      t.string :description

      t.timestamps null: false
    end
  end
end
