class AddPlaceOfInterestToPhoto < ActiveRecord::Migration
  def change
    add_reference :photos, :place_of_interest, index: true
    add_foreign_key :photos, :place_of_interests
  end
end
