class Photo < ActiveRecord::Base
  belongs_to :store
  belongs_to :place_of_interest
  mount_uploader :photo_path, PictureUploader

end
