# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

role_admin = Role.create!(role_name: ADMIN_USER)
role_app = Role.create!(role_name: APP_USER)
role_web = Role.create!(role_name: WEB_USER)
role_face = Role.create!(role_name: SOCIAL_NETWORK_USER)
d1 = DateTime.new(1989, 01, 19)
d2 = DateTime.new(1991, 11, 26)


admin = User.create!(email: "admin@groupmwt.com",
                     password: "foobar",
                     password_confirmation: "foobar",
                     role_id: role_admin.id,
                     born_date: d1,
                     country: "Venezuela, Bolivarian Republic of",
                     user_name: "Admin User",)

user1 = User.create!(email: "store@groupmwt.com",
                     password: "foobar",
                     password_confirmation: "foobar",
                     count_notification: 0,
                     notifications_number: 6,
                     born_date: d2,
                     role_id: role_web.id,
                     country: "Venezuela, Bolivarian Republic of",
                     user_name: "store"
)
=begin

user3 = User.create!(email: "app@groupmwt.com",
                     password: "foobar",
                     password_confirmation: "foobar",
                     count_notification: 0,
                     notifications_number: 6,
                     role_id: role_app.id,
                     country: "Venezuela, Bolivarian Republic of",
                     user_name: "app",
)

user_juan = User.create!(
    email: 'jcar@gmail.com',
    password: '11111111',
    password_confirmation: '11111111',
    born_date: d2,
    role_id: role_app.id,
    country: "Colombia",
    gender: 'female',
    user_name: 'Juan Carlos',
)


=end


user_ecu = User.create!(
    email: 'test@test.com',
    password: '11111111',
    password_confirmation: '11111111',
    role_id: role_app.id,
    born_date: d1,
    country: "Ecuador",
    province: 'Azuay',
    city: 'Camilo Ponce Enríquez',
    gender: 'Hombres', user_name: 'Juanito'
)


user_rafa = User.create!(
    email: 'rafa.a.solorzano@gmail.com',
    password: 'foobar',
    password_confirmation: 'foobar',
    role_id: role_app.id,
    born_date: d1,
    country: "Ecuador",
    province: 'Azuay',
    city: 'El Pan',
    gender: 'GLTBI', user_name: 'Rafael'
)


user_jesus = User.create!(
    email: 'jesus@mail.com',
    password: 'foobar',
    password_confirmation: 'foobar',
    role_id: role_app.id,
    born_date: d1,
    country: "Ecuador",
    province: 'Carchi',
    city: 'Mira',
    gender: 'Mujeres', user_name: 'jesus'
)

device1 = Device.create!(token_device: 'APA91bHlZ9fDwReF2Tmh4Saf8t6K5rsB0EqRJrDTApVZRs4tyXtgJzKmwtgpAKdPLGSJKJtjroz-5SSfC8nv5JlY-uma_ayCdShR6mKFLBVoK4MjIziIsc5oh7mrl9iMn0LqbeGQt43_',
                         operative_system: ANDROID, user_id: user_rafa.id)
device1.save!

user2 = User.create!(email: "store2@groupmwt.com",
                     password: "foobar",
                     password_confirmation: "foobar",
                     count_notification: 0,
                     notifications_number: 6,
                     role_id: role_web.id,
                     country: "Venezuela, Bolivarian Republic of",
                     user_name: "store2"
)


device2 = Device.create!(token_device: '82169abd3dd636224341dad0770fc66f299aafa39edd47839b2965770c21ff86',
                         operative_system: IOS, user_id: user_jesus.id)
device2.save!

kiosco1 = Store.create!(name: "kiosco 1", description: 'store one', kind: KIOSCO,
                        logo: File.open(Rails.root + "app/assets/images/local_lin.jpg"),
                        latitude: 0.000000000000000000, longitude: -78.45685187187507);

kiosco2 = Store.create!(name: "kiosco doña cleta", description: 'la mejor atencion directo en la playa, mesas, sillas, sombrillas, comida y bebidas',
                        kind: KIOSCO,
                        logo: File.open(Rails.root + "app/assets/images/local_aple.jpg"),
                        latitude: 0.000000000000000000, longitude: -78.45685187187507);


kiosco3 = Store.create!(name: "el chimuelo joe", description: 'store one', kind: KIOSCO,
                        logo: File.open(Rails.root + "app/assets/images/local_lin.jpg"),
                        latitude: 0.000000000000000000, longitude: -78.45685187187507);

kiosco4 = Store.create!(name: "petra", description: 'la mejor atencion directo en la playa, mesas, sillas, sombrillas, comida y bebidas',
                        kind: KIOSCO,
                        logo: File.open(Rails.root + "app/assets/images/local_aple.jpg"),
                        latitude: 0.000000000000000000, longitude: -78.45685187187507);


user1.store_id = kiosco1.id
user1.save!


rest1 = Store.create!(name: "La campana", description: 'Solo para entendidos de la comida gourmet,
                        encuentranos de 7pm-12pm', kind: RESTAURANT,
                      logo: File.open(Rails.root + "app/assets/images/spartan.png"),
                      latitude: 0.000000000000000000, longitude: -78.45685187187507);


rest2 = Store.create!(name: "RestoBar", description: 'El mejor Bar y restaurante de la playa,
                       acercate estamos abiertes de 8am a 12pm', kind: RESTAURANT,
                      logo: File.open(Rails.root + "app/assets/images/spartan.png"),
                      latitude: 0.000000000000000000, longitude: -78.45685187187507);


rest3= Store.create!(name: "rest3", description: 'Solo para entendidos de la comida gourmet,
                        encuentranos de 7pm-12pm', kind: RESTAURANT,
                     logo: File.open(Rails.root + "app/assets/images/spartan.png"),
                     latitude: 0.000000000000000000, longitude: -78.45685187187507);


rest4 = Store.create!(name: "Rest4", description: 'El mejor Bar y restaurante de la playa,
                       acercate estamos abiertes de 8am a 12pm', kind: RESTAURANT,
                      logo: File.open(Rails.root + "app/assets/images/spartan.png"),
                      latitude: 0.000000000000000000, longitude: -78.45685187187507);


rest5 = Store.create!(name: "Resto5", description: 'El mejor Bar y restaurante de la playa,
                       acercate estamos abiertes de 8am a 12pm', kind: RESTAURANT,
                      logo: File.open(Rails.root + "app/assets/images/spartan.png"),
                      latitude: 0.000000000000000000, longitude: -78.45685187187507);


photo1 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));
photo2 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));


photo3 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x150.jpg"));
photo4 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x150.jpg"));

photo5 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/300x200.jpg"));
photo6 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/300x200_2.jpg"));

photo7 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/300x200.jpg"));
photo8 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/300x200_2.jpg"));

photo9 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));
photo10 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));

photo11 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));
photo12 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));

photo13 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));
photo14 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));

photo15 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));
photo16 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/240x160.jpg"));


place1 = PlaceOfInterest.create!(name: 'Gym al aire libre', kind: DEPORTIVO,
                                 description: 'Tenemos barras, paralelas, pesas y circuitos de crossfit',
                                 latitude: -0.18477305209772163, longitude: -78.46646490898445)

place1.photos.append(photo10)
place1.photos.append(photo9)

place2 = PlaceOfInterest.create!(name: 'bailoterapia al aire libre', kind: CULTURAL,
                                 description: 'Tenemos el mejor instructor de salsa casino y bachata vallenata',
                                 latitude: -0.18477305209772163, longitude: -78.46646490898445
)

place2.photos.append(photo11)
place2.photos.append(photo12)

place3 = PlaceOfInterest.create!(name: 'Proyeccion al aire libre', kind: ENTRETENIMIENTO,
                                 description: 'Las proyecciones al aire libre cuentan con sillas, estacionamiento y bebidas',
                                 latitude: -0.18477305209772163, longitude: -78.46646490898445
)

place3.photos.append(photo13)
place3.photos.append(photo14)


place4 = PlaceOfInterest.create!(name: 'algo adicional ', kind: ADICIONALES,
                                 description: 'Adicional a los demas...',
                                 latitude: -0.18477305209772163, longitude: -78.46646490898445
)

place4.photos.append(photo15)
place4.photos.append(photo16)


rest1.photos.append(photo1);
rest1.photos.append(photo2);
rest2.photos.append(photo3);
rest2.photos.append(photo4);

photo17 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));
photo18 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));


rest3.photos.append(photo17);
rest3.photos.append(photo18);


photo19 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));
photo20 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));


rest4.photos.append(photo19);
rest4.photos.append(photo20);

photo21 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));
photo22 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));


rest5.photos.append(photo21);
rest5.photos.append(photo22);

rest1.save!
rest2.save!
rest3.save!
rest4.save!
rest5.save!

kiosco1.photos.append(photo5);
kiosco1.photos.append(photo6);

kiosco2.photos.append(photo7);
kiosco2.photos.append(photo8);


photo22 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));
photo23 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));


kiosco3.photos.append(photo22);
kiosco3.photos.append(photo23);


photo24 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));
photo25 = Photo.create!(photo_path: File.open(Rails.root + "app/assets/images/320x160.jpg"));


kiosco4.photos.append(photo24);
kiosco4.photos.append(photo25);

user2.store_id = rest1.id
user2.save!


promo1 = kiosco1.promotions.create!(title: "First promo",
                                    price: 20,
                                    promo_price: 15, count_notification: 0,
                                    conditions: "Debes llevar el cupon marcado con la promocion y tu documento de identidad",
                                    image: File.open(Rails.root + "app/assets/images/promo.jpg"),
                                    initial_date: "2016-02-11 12:00:00", finish_date: "2016-02-12 12:00:00")

promo2 = kiosco1.promotions.create!(title: "Second promo",
                                    price: 30,
                                    promo_price: 20,
                                    count_notification: 0,
                                    conditions: "debes acertar 3 de nuestras acertijos e ir bla bla bla ......",
                                    image: File.open(Rails.root + "app/assets/images/hot_promo.jpg"),
                                    initial_date: Date.today, finish_date: Date.tomorrow)


Event.create!(name: "Ghost Protocol", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
              projection: true,
              count_notification: 0, date: Date.tomorrow, user_id: user2.id, latitude: 8.598992, duration: 2,
              longitude: -71.1557195, city: "Mérida", country: "Venezuela", sponsor: "patrocinalo",
              image: File.open(Rails.root + "app/assets/images/mv1.jpg"),
              place_of_interest_id: place3.id)

Event.create!(name: "We Bought a Zoo", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
              projection: true,
              count_notification: 0, date: Date.tomorrow, user_id: user2.id, latitude: 8.598992, duration: 2,
              longitude: -71.1557195, city: "Mérida", country: "Venezuela", sponsor: "patrocinalo",
              image: File.open(Rails.root + "app/assets/images/mv2.jpg"),
              place_of_interest_id: place3.id)

Event.create!(name: "Avengers", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
              projection: true,
              count_notification: 0, date: Date.today, user_id: user2.id, latitude: 8.598992, duration: 2,
              longitude: -71.1557195, city: "Mérida", country: "Venezuela", sponsor: "patrocinalo",
              image: File.open(Rails.root + "app/assets/images/mv3.jpg"),
              place_of_interest_id: place3.id)

Event.create!(name: "Black Swan", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
              projection: true,
              count_notification: 0, date: Date.tomorrow, user_id: user2.id, latitude: 8.598992, duration: 2,
              longitude: -71.1557195, city: "Mérida", country: "Venezuela", sponsor: "patrocinalo",
              image: File.open(Rails.root + "app/assets/images/mv4.jpg"),
              place_of_interest_id: place3.id)

Event.create!(name: "127 Hours", detail: "Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
              projection: true,
              count_notification: 0, date: "2015-12-12 23:00:00", user_id: user2.id, latitude: 8.598992, duration: 2,
              longitude: -71.1557195, city: "Mérida", country: "Venezuela", sponsor: "patrocinalo",
              image: File.open(Rails.root + "app/assets/images/mv5.jpg"),
              place_of_interest_id: place3.id)


Event.create!(name: "Semana de cine japones", detail: 'la semana del cine japones,
 un evento donde se proyectaran los siguiente titulos lunes: hirosima, martes: sesneke',
              projection: true, count_notification: 0,
              date: Date.today, user_id: user2.id, latitude: 8.57831671789446,
              longitude: -71.1816043714111, city: "Mérida",
              country: "Venezuela", sponsor: "patrocinalo",
              image: File.open(Rails.root + "app/assets/images/rails.png"),
              place_of_interest_id: place3.id)

Event.create!(name: "Super Evento sensacional", detail: 'Super sabado sensacional actores invitados juan luis gerra y sus 4 40,
 luis fonsi y marco antonio, el chacal de la  trompera y juanga', projection: false, count_notification: 0,
              date: Date.tomorrow, user_id: user2.id, latitude: 8.57831671789446,
              longitude: -71.1816043714111, city: "Mérida",
              country: "Venezuela", sponsor: "patrocinalo2",
              image: File.open(Rails.root + "app/assets/images/boulevard-centro-comercial.jpg"),
              logo_sponsor: File.open(Rails.root + "app/assets/images/nick.png"),
              place_of_interest_id: place3.id)


Event.create!(name: "Eventasa", detail: 'hoy es el super evento magnifico', projection: false, count_notification: 0,
              date: "2015-12-14 3:30:00", user_id: user2.id, latitude: 8.57831671789446,
              longitude: -71.1816043714111, city: "Mérida",
              country: "Venezuela", sponsor: "Cantv",
              image: File.open(Rails.root + "app/assets/images/boulevard-centro-comercial.jpg"),
              logo_sponsor: File.open(Rails.root + "app/assets/images/nick.png"),
              place_of_interest_id: place3.id)


Comment.create!(user_id: 1, event_id: 2,
                body: 'exelente evento gracias a la gobernacion de pichincha por tan magnifica organizacion');


Comment.create!(user_id: 2, event_id: 2,
                body: 'Esta buenaza la proyeccion me gustaria, seria bueno tener una zona para discapacitados');


Comment.create!(user_id: 3, event_id: 2,
                body: 'Me gusto mucho, pueden mejorar el sistema de audio');

=begin

Comment.create!(user_id: 4, event_id: 2,
                body: 'Esta buenaza la proyeccion me gustaria, seria bueno tener una zona para discapacitados');

=end

#Review.create!(rate: 4, user_id: user1.id, promotion_id: promo1.id)

app = Rpush::Gcm::App.new
app.name = "android_app"
app.auth_key = "AIzaSyBoZ7du_9YyvqBLQu8OBazgTuEx83IjEhU"
app.connections = 1
app.save!

apple_notification = Rpush::Apns::App.new
apple_notification.name = "ios_app"
apple_notification.certificate = File.read('cert_production/ProductionCertificate.pem')
#apple_notification.certificate = File.read('cert_development/sandbox.pem')
#apple_notification.environment = "sandbox" # APNs environment.
apple_notification.environment = "production" # APNs environment.
apple_notification.password = "disenos88"
apple_notification.connections = 1
apple_notification.save!

Canton.create!(name: 'Camilo Ponce Enríquez', province: 'Azuay')
Canton.create!(name: 'Chordeleg', province: 'Azuay')
Canton.create!(name: 'El Pan', province: 'Azuay')
Canton.create!(name: 'Girón', province: 'Azuay')
Canton.create!(name: 'Guachapala', province: 'Azuay')
Canton.create!(name: 'Gualaceo', province: 'Azuay')
Canton.create!(name: 'Nabón', province: 'Azuay')
Canton.create!(name: 'Oña', province: 'Azuay')
Canton.create!(name: 'Paute', province: 'Azuay')
Canton.create!(name: 'Pucará', province: 'Azuay')
Canton.create!(name: 'San Fernando', province: 'Azuay')
Canton.create!(name: 'Santa Isabel', province: 'Azuay')
Canton.create!(name: 'Sevilla de Oro', province: 'Azuay')
Canton.create!(name: 'Sigsig', province: 'Azuay')


Canton.create!(name: 'Caluma', province: 'Bolívar')
Canton.create!(name: 'Chillanes', province: 'Bolívar')
Canton.create!(name: 'Chimbo', province: 'Bolívar')
Canton.create!(name: 'Echeandía', province: 'Bolívar')
Canton.create!(name: 'Guaranda', province: 'Bolívar')
Canton.create!(name: 'Las Naves', province: 'Bolívar')
Canton.create!(name: 'San Miguel', province: 'Bolívar')


Canton.create!(name: 'Azogues', province: 'Cañar')
Canton.create!(name: 'La Troncal', province: 'Cañar')
Canton.create!(name: 'Biblián', province: 'Cañar')
Canton.create!(name: 'Cañar', province: 'Cañar')
Canton.create!(name: 'Déleg', province: 'Cañar')
Canton.create!(name: 'El Tambo', province: 'Cañar')
Canton.create!(name: 'Suscal', province: 'Cañar')

Canton.create!(name: 'Bolívar', province: 'Carchi')
Canton.create!(name: 'Espejo', province: 'Carchi')
Canton.create!(name: 'Mira', province: 'Carchi')
Canton.create!(name: 'Montúfar', province: 'Carchi')
Canton.create!(name: 'San Pedro de Huaca', province: 'Carchi')
Canton.create!(name: 'Tulcán', province: 'Carchi')

Canton.create!(name: 'Alausí', province: 'Chimborazo')
Canton.create!(name: 'Chambo', province: 'Chimborazo')
Canton.create!(name: 'Chunchi', province: 'Chimborazo')
Canton.create!(name: 'Colta', province: 'Chimborazo')
Canton.create!(name: 'Cumandá', province: 'Chimborazo')
Canton.create!(name: 'Guamote', province: 'Chimborazo')
Canton.create!(name: 'Guano', province: 'Chimborazo')
Canton.create!(name: 'Pallatanga', province: 'Chimborazo')
Canton.create!(name: 'Penipe', province: 'Chimborazo')
Canton.create!(name: 'Riobamba', province: 'Chimborazo')

Canton.create!(name: 'La Maná', province: 'Cotopaxi')
Canton.create!(name: 'Latacunga', province: 'Cotopaxi')
Canton.create!(name: 'Pangua', province: 'Cotopaxi')
Canton.create!(name: 'Pujilí', province: 'Cotopaxi')
Canton.create!(name: 'Salcedo', province: 'Cotopaxi')
Canton.create!(name: 'Saquisilí', province: 'Cotopaxi')
Canton.create!(name: 'Sigchos', province: 'Cotopaxi')

Canton.create!(name: 'Arenillas', province: 'El Oro')
Canton.create!(name: 'Atahualpa', province: 'El Oro')
Canton.create!(name: 'Balsas', province: 'El Oro')
Canton.create!(name: 'Chilla', province: 'El Oro')
Canton.create!(name: 'El Guabo', province: 'El Oro')
Canton.create!(name: 'Huaquillas', province: 'El Oro')
Canton.create!(name: 'Machala', province: 'El Oro')
Canton.create!(name: 'Marcabelí', province: 'El Oro')
Canton.create!(name: 'Pasaje', province: 'El Oro')
Canton.create!(name: 'Piñas', province: 'El Oro')
Canton.create!(name: 'Portovelo', province: 'El Oro')
Canton.create!(name: 'Santa Rosa', province: 'El Oro')
Canton.create!(name: 'Zaruma', province: 'El Oro')

Canton.create!(name: 'Atacames', province: 'Esmeraldas')
Canton.create!(name: 'Eloy Alfaro', province: 'Esmeraldas')
Canton.create!(name: 'Esmeraldas', province: 'Esmeraldas')
Canton.create!(name: 'Muisne', province: 'Esmeraldas')
Canton.create!(name: 'Quinindé', province: 'Esmeraldas')
Canton.create!(name: 'Río Verde', province: 'Esmeraldas')
Canton.create!(name: 'San Lorenzo', province: 'Esmeraldas')

Canton.create!(name: 'Isabela', province: 'Galápagos')
Canton.create!(name: 'San Cristóbal', province: 'Galápagos')
Canton.create!(name: 'Santa Cruz', province: 'Galápagos')

Canton.create!(name: 'Alfredo', province: 'Guayas')
Canton.create!(name: 'Balao', province: 'Guayas')
Canton.create!(name: 'Balzar', province: 'Guayas')
Canton.create!(name: 'Colimes', province: 'Guayas')
Canton.create!(name: 'Coronel Marcelino Maridueña', province: 'Guayas')
Canton.create!(name: 'Daule', province: 'Guayas')
Canton.create!(name: 'Durán', province: 'Guayas')
Canton.create!(name: 'El Empalme', province: 'Guayas')
Canton.create!(name: 'El Triunfo', province: 'Guayas')
Canton.create!(name: 'General Antonio Elizalde (Bucay)', province: 'Guayas')
Canton.create!(name: 'Guayaquil', province: 'Guayas')
Canton.create!(name: 'Isidro Ayora', province: 'Guayas')
Canton.create!(name: 'Lomas de Sargentillo', province: 'Guayas')
Canton.create!(name: 'Milagro', province: 'Guayas')
Canton.create!(name: 'Naranjal', province: 'Guayas')
Canton.create!(name: 'Naranjito', province: 'Guayas')
Canton.create!(name: 'Nobol', province: 'Guayas')
Canton.create!(name: 'Palestina', province: 'Guayas')
Canton.create!(name: 'Pedro Carbo', province: 'Guayas')
Canton.create!(name: 'Playas (General Villamil Playas)', province: 'Guayas')
Canton.create!(name: 'Salitre', province: 'Guayas')
Canton.create!(name: 'Samborondón', province: 'Guayas')
Canton.create!(name: 'Santa Lucía', province: 'Guayas')
Canton.create!(name: 'Simón Bolívar', province: 'Guayas')
Canton.create!(name: 'La Troncal', province: 'Guayas')
Canton.create!(name: 'Yaguachi', province: 'Guayas')

Canton.create!(name: 'Antonio Ante', province: 'Imbabura')
Canton.create!(name: 'Cotacachi', province: 'Imbabura')
Canton.create!(name: 'Ibarra', province: 'Imbabura')
Canton.create!(name: 'Otavalo', province: 'Imbabura')
Canton.create!(name: 'Pimampiro', province: 'Imbabura')
Canton.create!(name: 'San Miguel de Urcuquí', province: 'Imbabura')

Canton.create!(name: 'Calvas', province: 'Loja')
Canton.create!(name: 'Catamayo', province: 'Loja')
Canton.create!(name: 'Celica', province: 'Loja')
Canton.create!(name: 'Chaguarpamba', province: 'Loja')
Canton.create!(name: 'Espíndola', province: 'Loja')
Canton.create!(name: 'Gonzanamá', province: 'Loja')
Canton.create!(name: 'Loja', province: 'Loja')
Canton.create!(name: 'Macará', province: 'Loja')
Canton.create!(name: 'Olmedo', province: 'Loja')
Canton.create!(name: 'Paltas', province: 'Loja')
Canton.create!(name: 'Pindal', province: 'Loja')
Canton.create!(name: 'Puyango', province: 'Loja')
Canton.create!(name: 'Quilanga', province: 'Loja')
Canton.create!(name: 'Saraguro', province: 'Loja')
Canton.create!(name: 'Sozoranga', province: 'Loja')
Canton.create!(name: 'Zapotillo', province: 'Loja')

Canton.create!(name: 'Baba', province: 'Los Ríos')
Canton.create!(name: 'Babahoyo', province: 'Los Ríos')
Canton.create!(name: 'Buena Fé', province: 'Los Ríos')
Canton.create!(name: 'Mocache', province: 'Los Ríos')
Canton.create!(name: 'Montalvo', province: 'Los Ríos')
Canton.create!(name: 'Palenque', province: 'Los Ríos')
Canton.create!(name: 'Pueblo Viejo', province: 'Los Ríos')
Canton.create!(name: 'Quevedo', province: 'Los Ríos')
Canton.create!(name: 'Quinsaloma', province: 'Los Ríos')
Canton.create!(name: 'Urdaneta', province: 'Los Ríos')
Canton.create!(name: 'Valencia', province: 'Los Ríos')
Canton.create!(name: 'Ventanas', province: 'Los Ríos')
Canton.create!(name: 'Vinces', province: 'Los Ríos')


Canton.create!(name: 'Bolívar', province: 'Manabí')
Canton.create!(name: 'Chone', province: 'Manabí')
Canton.create!(name: 'El Carmen', province: 'Manabí')
Canton.create!(name: 'Flavio Alfaro', province: 'Manabí')
Canton.create!(name: 'Jama', province: 'Manabí')
Canton.create!(name: 'Jaramijó', province: 'Manabí')
Canton.create!(name: 'Jipijapa', province: 'Manabí')
Canton.create!(name: 'Junín', province: 'Manabí')
Canton.create!(name: 'Manta', province: 'Manabí')
Canton.create!(name: 'Montecristi', province: 'Manabí')
Canton.create!(name: 'Olmedo', province: 'Manabí')
Canton.create!(name: 'Paján', province: 'Manabí')
Canton.create!(name: 'Pedernales', province: 'Manabí')
Canton.create!(name: 'Pichincha', province: 'Manabí')
Canton.create!(name: 'Portoviejo', province: 'Manabí')
Canton.create!(name: 'Puerto López', province: 'Manabí')
Canton.create!(name: 'Rocafuerte', province: 'Manabí')
Canton.create!(name: 'San Vicente', province: 'Manabí')
Canton.create!(name: 'Santa Ana', province: 'Manabí')
Canton.create!(name: 'Sucre', province: 'Manabí')
Canton.create!(name: 'Tosagua', province: 'Manabí')
Canton.create!(name: 'Veinticuatro de Mayo', province: 'Manabí')

Canton.create!(name: 'Gualaquiza', province: 'Morona-Santiago')
Canton.create!(name: 'Huamboya', province: 'Morona-Santiago')
Canton.create!(name: 'Limón Indanza', province: 'Morona-Santiago')
Canton.create!(name: 'Logroño', province: 'Morona-Santiago')
Canton.create!(name: 'Morona', province: 'Morona-Santiago')
Canton.create!(name: 'Pablo Sexto', province: 'Morona-Santiago')
Canton.create!(name: 'Palora', province: 'Morona-Santiago')
Canton.create!(name: 'San Juan Bosco', province: 'Morona-Santiago')
Canton.create!(name: 'Santiago de Méndez (Santiago)', province: 'Morona-Santiago')
Canton.create!(name: 'Sucúa', province: 'Morona-Santiago')
Canton.create!(name: 'Taisha', province: 'Morona-Santiago')
Canton.create!(name: 'Tiwintza', province: 'Morona-Santiago')

Canton.create!(name: 'Archidona', province: 'Napo')
Canton.create!(name: 'Carlos Julio Arosemena Tola Tola', province: 'Napo')
Canton.create!(name: 'El Chaco', province: 'Napo')
Canton.create!(name: 'Quijos', province: 'Napo')
Canton.create!(name: 'Tena', province: 'Napo')

Canton.create!(name: 'Aguarico', province: 'Orellana')
Canton.create!(name: 'Francisco de Orellana', province: 'Orellana')
Canton.create!(name: 'Joya de los Sachas', province: 'Orellana')
Canton.create!(name: 'Loreto', province: 'Orellana')

Canton.create!(name: 'Loreto', province: 'Pastaza')
Canton.create!(name: 'Arajuno', province: 'Pastaza')
Canton.create!(name: 'Mera', province: 'Pastaza')
Canton.create!(name: 'Pastaza', province: 'Pastaza')
Canton.create!(name: 'Santa Clara', province: 'Pastaza')


Canton.create!(name: 'Cayambe', province: 'Pichincha')
Canton.create!(name: 'Mejía', province: 'Pichincha')
Canton.create!(name: 'Pedro Moncayo', province: 'Pichincha')
Canton.create!(name: 'Pedro Vicente', province: 'Pichincha')
Canton.create!(name: 'Puerto Quito', province: 'Pichincha')
Canton.create!(name: 'Quito', province: 'Pichincha')
Canton.create!(name: 'Puerto Quito', province: 'Pichincha')
Canton.create!(name: 'Rumiñahui', province: 'Pichincha')
Canton.create!(name: 'San Miguel', province: 'Pichincha')

Canton.create!(name: 'La Libertad', province: 'Santa Elena')
Canton.create!(name: 'Salinas', province: 'Santa Elena')
Canton.create!(name: 'Santa Elena', province: 'Santa Elena')

Canton.create!(name: 'Santo Domingo de los Colorados (Santo Domingo)', province: 'Santo Domingo de los Tsáchilas')
Canton.create!(name: 'La Concordia', province: 'Santo Domingo de los Tsáchilas')

Canton.create!(name: 'Cascales', province: 'Sucumbíos')
Canton.create!(name: 'Cuyabeno', province: 'Sucumbíos')
Canton.create!(name: 'Gonzalo Pizarro', province: 'Sucumbíos')
Canton.create!(name: 'Lago Agrio', province: 'Sucumbíos')
Canton.create!(name: 'Putumayo', province: 'Sucumbíos')
Canton.create!(name: 'Shushufindi', province: 'Sucumbíos')
Canton.create!(name: 'Sucumbíos', province: 'Sucumbíos')

Canton.create!(name: 'Ambato', province: 'Tungurahua')
Canton.create!(name: 'Baños', province: 'Tungurahua')
Canton.create!(name: 'Cevallos', province: 'Tungurahua')
Canton.create!(name: 'Mocha', province: 'Tungurahua')
Canton.create!(name: 'Patate', province: 'Tungurahua')
Canton.create!(name: 'Pelileo', province: 'Tungurahua')
Canton.create!(name: 'Píllaro', province: 'Tungurahua')
Canton.create!(name: 'Quero', province: 'Tungurahua')
Canton.create!(name: 'Tisaleo', province: 'Tungurahua')

Canton.create!(name: 'Centinela del Cóndor', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Chinchipe', province: 'Zamora-Chinchipe')
Canton.create!(name: 'El Pangui', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Nangaritza', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Palanda', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Paquisha', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Yacuambi', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Yantzaza', province: 'Zamora-Chinchipe')
Canton.create!(name: 'Zamora', province: 'Zamora-Chinchipe')

