json.id @event.id
json.name @event.name
json.detail @event.detail
json.date @event.date
json.projection @event.projection
json.city @event.city
json.image request.protocol + request.host_with_port + @event.image.to_s
json.latitude @event.latitude
json.longitude @event.longitude
json.sponsor @event.sponsor
json.logo_sponsor request.protocol + request.host_with_port + @event.logo_sponsor.to_s
json.duration @event.duration
json.comments_count @event.comments.count