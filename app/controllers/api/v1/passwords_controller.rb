class Api::V1::PasswordsController < Api::ApiController
  def create
    @user = User.find_by(email: params[:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      render :status => 200, :json => {:success => true}

    else
      render :json => {:success => false}
    end
  end
end


