class AddCountryToPlaceOfInterest < ActiveRecord::Migration
  def change
    add_column :place_of_interests, :country, :string
  end
end
