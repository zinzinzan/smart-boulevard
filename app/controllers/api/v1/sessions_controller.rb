class Api::V1::SessionsController < Api::ApiController

  respond_to :json, :js

  def new
    if logged_in?
      #if log in
      redirect_to root_url
    end
  end

  def create
    @user = User.find_by(email: params[:email].downcase)
    if @user.nil?
      render json: {sucess: '0'}
      return
    end
    @device = @user.device

    if @device == nil
      @device = Device.new
      @device.token_device = params[:token_device]
      @device.operative_system = params[:os]
      @device.user = @user
      @device.save
    else
      @device.destroy!
      @device = Device.new
      @device.token_device = params[:token_device]
      @device.operative_system = params[:os]
      @device.user = @user
      @device.save
    end


    if @user.role.role_name == SOCIAL_NETWORK_USER
      render json: {sucess: '0'}
      return
    end
    if @user && @user.authenticate(params[:password])
      return
    else
      render json: {sucess: '0'}
    end
  end


  def destroy
    log_out if logged_in?
    # redirect_to root_url;
  end

end
