class StaticPagesController < ApplicationController
  layout 'login', only: :password_changed

  def home
    if logged_in?


      # @users = User.joins("LEFT JOIN Roles ON users.id = roles.id").where('roles.name = :role_name',role_name: 'App_user')
      # @connected_users = User.joins("LEFT JOIN Roles ON users.id = roles.id").where('users.connected = :connected AND roles.name = :role_name', connected: true,role_name: 'App_user')
      if current_user.admin
        uptime = `uptime`
        @server_uptime = uptime.to_s[9, 6]

        @connected_users = User.where('users.connected = :connected', connected: true)
        @users = User.all
        @events = Event.all
        @places = PlaceOfInterest.all
        @stores = Store.all
        @promos = Promotion.all
        connection = ActiveRecord::Base.connection
        @count = connection.execute("SELECT COUNT(*) FROM rpush_notifications;")
        @count = @count[0]["count"]
      else
        @promos = Promotion.where('store_id = :store_id_param', store_id_param: current_user.store.id)
        @count = current_user.count_notification
      end




    end
  end

  def help
  end

  def about
  end

  def password_changed
    redirect_to root_path unless request.referer =~ /\/password_resets\/.*/
  end

end
