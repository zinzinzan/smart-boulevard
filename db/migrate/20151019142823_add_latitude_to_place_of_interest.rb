class AddLatitudeToPlaceOfInterest < ActiveRecord::Migration
  def change
    add_column :place_of_interests, :latitude, :float
  end
end
