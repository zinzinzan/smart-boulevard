class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :photo_path
      t.references :store, index: true

      t.timestamps null: false
    end
    add_foreign_key :photos, :stores
  end
end
