json.places @place_of_interests do |place|
  json.id place.id
  json.name place.name
  json.latitude place.latitude
  json.longitude place.longitude

  json.photos place.photos do |photo|
    json.photo request.protocol + request.host_with_port + photo.photo_path.to_s
  end
end