class AddCountNotificationToUser < ActiveRecord::Migration
  def change
    add_column :users, :count_notification, :integer
  end
end
