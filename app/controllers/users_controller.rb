class UsersController < ApplicationController

  include CanCan::ControllerAdditions

  load_and_authorize_resource

  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  # before_action :correct_user, only: [:edit, :update]

  # before_action :admin_user, only: [:destroy, :index]


  def subcategories_options
    render partial: 'users/admin_index'
  end


  def show
    @user = User.find(params[:id])
  end

#------------------------------------------------------------------

  def new
    @user = User.new
  end

#------------------------------------------------------------------

  def create
    @user= User.new(user_params)
    @role = Role.find(params[:role][:id])
    @user.role = @role
    if @user.save
      if params[:user][:store_id].present?
        @store = Store.find(params[:user][:store_id])
        @user.store = @store
        @user.save
      end
      if params[:user][:notifications_number].present?
        @user.count_notification = 0
        @user.save
      end
      redirect_to users_path

    else
      render 'new';
    end

#------------------------------------------------------------------

  end

  def edit
    @user = User.find(params[:id])
  end

#------------------------------------------------------------------

  def update
    @user = User.find(params[:id])


    if @user.update_attributes(user_params)
      if params[:role][:role_id].present?
        @role = Role.find(params[:role][:role_id])
        @user.role = @role
        @user.save
      end
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

#------------------------------------------------------------------

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:success] = "User deleted"
    @users = User.paginate(page: params[:page], per_page: 4)

  end

#------------------------------------------------------------------

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

#------------------------------------------------------------------

  def index
    if params[:per_page].present?
      User.per_page = params[:per_page]
    end
    @users = User.paginate(page: params[:page])
  end

#------------------------------------------------------------------

  def admin_user
    if !current_user.admin?
      flash[:warning] = "Usted no esta autorizado para ingresar a esta seccion"
      redirect_to root_url
    end

  end

#------------------------------------------------------------------

  private

  def user_params
    params.require(:user).permit(:user_name, :email, :notifications_number, :password,
                                 :password_confirmation, :store_id)
  end

  def role_params
    params.permit(:role_id)
  end


end
