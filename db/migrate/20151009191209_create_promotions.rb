class CreatePromotions < ActiveRecord::Migration
  def change
    create_table :promotions do |t|
      t.integer :rate
      t.string :title
      t.string :image
      t.float :price
      t.float :promo_price
      t.timestamps null: false
    end
    add_index :promotions, [:created_at]
  end
end
