var ready = function () {
    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(function(position)
        {
            if (!$('.form-lat').val())
            {
                clat = position.coords.latitude;
                clng = position.coords.longitude;

                //console.log("Lat Inicial:", clat);
                //console.log("Lng Inicial:", clng);

                $('.form-lat').val(position.coords.latitude);
                $('.form-lng').val(position.coords.longitude);

                var myLatlng = {lat: clat, lng: clng};
                //console.log ("myLatlng:", myLatlng);

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 15,
                    center: {lat: clat, lng: clng}
                });
                //console.log("map: ",map);

                marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: {lat: clat, lng: clng}
                });
            }
            else
            {
                clat = $('.form-lat').val();
                clng = $('.form-lng').val();

                clat = Number(clat);
                clng = Number(clng);

                //console.log("Lat DB:", clat);
                //console.log("Lng DB:", clng);

                current_pos = new google.maps.LatLng(clat, clng);
                //console.log ("current_pos:", current_pos);

                var myLatlng = {lat: clat, lng: clng};
                //console.log ("myLatlng:", myLatlng);

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: {lat: clat, lng: clng}
                });
                //console.log("map: ",map);

                marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    animation: google.maps.Animation.DROP,
                    position: {lat: clat, lng: clng}
                });
            }

            latlng = new google.maps.LatLng(clat, clng);
            geocoder = new google.maps.Geocoder();

            geocoder.geocode({'latLng': latlng}, function(results, status)
            {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                        for (var i = 0; i < results.length; i++) {
                            if (results[i].types[0] === "locality") {
                                var country_post = results[i].address_components.length-1;
                                var city = results[i].address_components[0].long_name;
                                var country = results[i].address_components[country_post].long_name;

                                //console.log("City Inicial:", city);
                                //console.log("Country Inicial:", country);

                                $('.form-city').val(city);
                                $('.form-country').val(country);
                            }
                        }
                    }
                    else {console.log("No reverse geocode results.")}
                }
                else {console.log("Geocoder failed: " + status)}
            });

            /** @type {!HTMLInputElement} */
            var input = (document.getElementById('pac-input'));

            var types = document.getElementById('type-selector');
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

            latlng = new google.maps.LatLng(clat, clng);

            map.addListener('center_changed', function() {
                window.setTimeout(function() {
                    map.panTo(marker.getPosition());
                }, 3000);
                //console.log("marker.getPosition(): ", marker.getPosition());

                latlng   = new google.maps.LatLng(marker.getPosition().lat(), marker.getPosition().lng());
                geocoder.geocode({'latLng': latlng}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[1]) {
                            for (var i = 0; i < results.length; i++) {
                                if (results[i].types[0] === "locality"){
                                    //console.log("Ciudad: ",results[i].address_components.length);

                                    var country_post = results[i].address_components.length-1;
                                    var city = results[i].address_components[0].long_name;
                                    var country = results[i].address_components[country_post].long_name;

                                    //console.log("City Inicial:", city);
                                    //console.log("Country Inicial:", country);

                                    $('.form-city').val(city);
                                    $('.form-country').val(country);
                                }
                            }
                        }
                        else {console.log("No reverse geocode results.")}
                    }
                    else {console.log("Geocoder failed: " + status)}
                });
            });

            google.maps.event.addListener(marker,'drag',function(event) {
                window.setTimeout(function() {
                    map.panTo(marker.getPosition());

                    //console.log("New Drag Lat:", event.latLng.lat());
                    //console.log("New Drag Lng:", event.latLng.lng());

                    $('.form-lat').val(event.latLng.lat());
                    $('.form-lng').val(event.latLng.lng());
                }, 3000);
            });


            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);

            var infowindow = new google.maps.InfoWindow();


            autocomplete.addListener('place_changed', function() {
                infowindow.close();
                marker.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                }
                else
                {
                    map.setCenter(place.geometry.location);
                    map.setZoom(16);  // Why 17? Because it looks good.

                    //console.log("New Place:", place.geometry.location);
                    //console.log("New Lat:", place.geometry.location.lat());
                    //console.log("New Lng:", place.geometry.location.lng());

                    $('.form-lat').val(place.geometry.location.lat());
                    $('.form-lng').val(place.geometry.location.lng());

                    //console.log("place.geometry.location: ",place.geometry.location);

                    latlng   = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());

                    //console.log("New LatLng:", latlng);

                    geocoder.geocode({'latLng': latlng}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                for (var i = 0; i < results.length; i++) {
                                    if (results[i].types[0] === "locality"){
                                        //console.log("Ciudad: ",results[i].address_components.length);

                                        var country_post = results[i].address_components.length-1;
                                        var city = results[i].address_components[0].long_name;
                                        var country = results[i].address_components[country_post].long_name;

                                        //console.log("New City:", city);
                                        //console.log("New Country:", country);

                                        $('.form-city').val(city);
                                        $('.form-country').val(country);
                                    }
                                }
                            }
                            else {console.log("No reverse geocode results.")}
                        }
                        else {console.log("Geocoder failed: " + status)}
                    });
                }

                marker.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));

                marker.setPosition(place.geometry.location);
                marker.setVisible(true);

                var address = '';
                if (place.address_components)
                {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker);
            });

            // Sets a listener on a radio button to change the filter type on Places
            // Autocomplete.
            function setupClickListener(id, types)
            {
                var radioButton = document.getElementById(id);
                radioButton.addEventListener('click', function() {
                    autocomplete.setTypes(types);
                });
            }
        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    }
    else
    {
    }
}
function toggleBounce()
{
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    }
    else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}
$(document).ready(ready);
$(document).on("page:load", ready);