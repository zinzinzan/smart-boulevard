class AddLongitudeToPlaceOfInterest < ActiveRecord::Migration
  def change
    add_column :place_of_interests, :longitude, :float
  end
end
