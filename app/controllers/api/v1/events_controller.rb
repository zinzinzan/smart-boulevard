class Api::V1::EventsController < Api::ApiController

  #----------------------------------------------------------------------
  def show
    @event = Event.find(params[:id])
  end

  #----------------------------------------------------------------------

  def comments
    @event = Event.find(params[:id])
    @comments = @event.comments;
  end

  #----------------------------------------------------------------------

  def index_projections
    @projections = Event.where("projection = ?", true)
  end

  #----------------------------------------------------------------------

  def index_events
    date_param = DateTime.now

    begin
      @events = Event.where("projection = :projection_param AND date > :date_param",
                            {projection_param: false, date_param: date_param}).order(date: :desc)

    rescue ActiveRecord::RecordNotFound
      # handle not found error
      render json: {error: 'no se encontro evento alguno en BD'}
      return
    end

  end

  #----------------------------------------------------------------------

  def last
    date_param = DateTime.now

    begin
      @events = Event.where("projection = :projection_param AND date > :date_param",
                            {projection_param: false, date_param: date_param}).order(date: :desc)

    rescue ActiveRecord::RecordNotFound
      # handle not found error
      render json: {error: 'no se encontro evento alguno en BD'}
      return
    end


  end

  #----------------------------------------------------------------------

  def last_projections
    date_param = DateTime.now

    begin
      @projections = Event.where("projection = :projection_param AND date > :date_param",
                                 {projection_param: true, date_param: date_param}).order(date: :desc)
    rescue ActiveRecord::RecordNotFound
      render json: {error: 'no se encontro projections en BD'}
      return
    end


  end

  #----------------------------------------------------------------------

  def paginate_event
    current_page = Integer(params[:pag], 10)
    per_page = 2
    records_fetch_point = (current_page - 1) * per_page

    eve = Event.all.where("date >= :today", {today: Date.today}).limit(per_page).offset(records_fetch_point)
    promo = Promotion.all.where("initial_date >=  :today",
                                {today: Date.today}).limit(per_page).offset(records_fetch_point);
    @events = eve + promo

    #events out of date so when first call to service we delivery last 2 of each objects
    if @events.length == 0 and current_page == 1
      eve = Event.last(2)
      promo = Promotion.last(2)
      @events = eve + promo
    end

  end

end