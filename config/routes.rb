Rails.application.routes.draw do

  get 'notifications/new'

  post 'notifications/create' => 'notifications#create'

  get 'reports/show'

  get 'reports/export_to_excel_users'

  post 'reports/filter' => 'reports#filter'

  root 'static_pages#home'
  get 'successfull_password_changed', to: 'static_pages#password_changed'
  #root 'sessions#new'

  get 'help' => 'static_pages#help';

  get 'about' => 'static_pages#about';

  get 'contact' => 'static_pages#contact';

  get 'signup' => 'users#new'

  get 'login' => 'sessions#new'

  post 'login' => 'sessions#create'

  delete 'logout' => 'sessions#destroy'

  # get 'api/v1/sessions/login' => 'api/v1/sessions#create',  :defaults => { :format => 'json' }


  get 'api/v1/users/create' => 'api/v1/users#create', :defaults => {:format => 'json'}

  post '/api/v1/sessions/create_fb' => 'api/v1/sessions#create_fb', :defaults => {:format => 'json'}


  # post "/api/v1/sessions/login" => 'api/v1/sessions#create',  :defaults => { :format => 'json' }

  match '*any' => 'api/api#options', :via => [:options]
  #admin borrar y editar promos , ojo poner antes de resource :users!!!!
  get '/users/subcategories_options' => 'users#subcategories_options'

  get '/orders/subregion_options' => 'notifications#subregion_options'

  get '/cantons/subregion_options' => 'notifications#subsubregion_options'

  post "/promotions/filter" => 'promotions#filter'

  post "/events/filter" => 'events#filter'

  post "/stores/filter" => 'stores#filter'

  post "/place_of_interests/filter" => 'place_of_interests#filter'

  resources :promotions

  resources :users

  get 'admin_promotions' => 'promotions#admin_index'

  resources :events

  resources :stores

  resources :place_of_interests

  resources :password_resets, only: [:edit, :update]

  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :sessions
      resources :users
      resources :promotions
      resources :events
      resources :stores
      resources :place_of_interests
      post 'reset_password', to: 'passwords#create'
      post 'update_token_device', to: 'users#update_user_device'

    end
  end

  #create a comment
  post '/api/v1/comments/:event_id/:body' => 'api/v1/comments#create'
  #rate a promo
  post '/api/v1/reviews/:promo_id/rate/:rate' => 'api/v1/reviews#vote'

  #update token_device


  #get mi promo rate
  get '/api/v1/reviews/:promo_id' => 'api/v1/reviews#show_user_vote'

  get '/api/v1/get_events' => 'api/v1/events#last'

  get '/api/v1/get_last_events' => 'api/v1/events#last'

  get '/api/v1/get_last_projections' => 'api/v1/events#last_projections'

  get '/api/v1/get_last_promotions' => 'api/v1/promotions#last'

  get '/api/v1/get_countries' => 'api/v1/country#index'

  get '/api/v1/comments/:event_id/:index' => 'api/v1/comments#index_for_event'

  get '/api/v1/get_restaurants' => 'api/v1/stores#index_rest'

  get '/api/v1/get_kioscos' => 'api/v1/stores#index_kiosco'

  get '/api/v1/get_enter' => 'api/v1/place_of_interests#index_enter'
  get '/api/v1/get_cult' => 'api/v1/place_of_interests#index_cult'
  get '/api/v1/get_depor' => 'api/v1/place_of_interests#index_depor'
  get '/api/v1/get_adic' => 'api/v1/place_of_interests#index_adic'

# Restaurants paginate
  get '/api/v1/restaurant/:pag' => 'api/v1/stores#paginate_rest'

# Kiosco paginate
  get '/api/v1/kiosco/:pag' => 'api/v1/stores#paginate_kiosco'

# event paginate
  get '/api/v1/paginate_event/:pag' => 'api/v1/events#paginate_event'


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
