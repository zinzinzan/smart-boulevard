json.promotions @promotions do |promotion|
  json.id promotion.id
  json.title promotion.title
  json.price promotion.price
  json.promo_price promotion.promo_price
  json.image request.protocol + request.host_with_port + promotion.image.to_s
end