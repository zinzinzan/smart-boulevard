class AddNotificationBodyToEvent < ActiveRecord::Migration
  def change
    add_column :events, :notification_body, :string
  end
end
