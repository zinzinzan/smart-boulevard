class Device < ActiveRecord::Base
  belongs_to :user
  validates(:token_device, presence: true)
  validates(:operative_system, presence: true)
  validates(:user_id, presence: true)
end
