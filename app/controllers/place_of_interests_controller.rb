class PlaceOfInterestsController < ApplicationController


  def index
    if params[:per_page].present?
      PlaceOfInterest.per_page = params[:per_page]
    end
    if params[:kind] == DEPORTIVO
      @place_of_interests = PlaceOfInterest.where('place_of_interests.kind = :kind_param', kind_param: params[:kind]).paginate(page: params[:page])
    end
    if params[:kind] == CULTURAL
      @place_of_interests = PlaceOfInterest.where('place_of_interests.kind = :kind_param', kind_param: params[:kind]).paginate(page: params[:page])
    end
    if params[:kind] == ENTRETENIMIENTO
      @place_of_interests = PlaceOfInterest.where('place_of_interests.kind = :kind_param', kind_param: params[:kind]).paginate(page: params[:page])
    end
    if params[:kind] == ADICIONALES
      @place_of_interests = PlaceOfInterest.where('place_of_interests.kind = :kind_param', kind_param: params[:kind]).paginate(page: params[:page])
    end
  end

  def new
    @place_of_interest = PlaceOfInterest.new
    @photos = []
    for i in 0..3
      @photo = Photo.new
      @photos.append(@photo)
    end
  end

  def create
    @place_of_interest = PlaceOfInterest.new(place_of_interest_params)
    @photos = []
    for i in 0..3
      @photo = Photo.new
      @photos.append(@photo)
    end

    @place_of_interest.kind = params[:kind]

    if @place_of_interest.save
      if params[:place_photo_0].present?
        @image0 = @place_of_interest.photos.create(photo_path: params[:place_photo_0], place_of_interest_id: @place_of_interest.id)
        @image0.save
      end

      if params[:place_photo_1].present?
        @image1 = @place_of_interest.photos.create(photo_path: params[:place_photo_1], place_of_interest_id: @place_of_interest.id)
        @image1.save
      end
      if params[:place_photo_2].present?
        @image2 = @place_of_interest.photos.create(photo_path: params[:place_photo_2], place_of_interest_id: @place_of_interest.id)
        @image2.save
      end

      if params[:place_photo_3].present?
        @image3 = @place_of_interest.photos.create(photo_path: params[:place_photo_3], place_of_interest_id: @place_of_interest.id)
        @image3.save
      end

      redirect_to place_of_interests_path(kind: params[:kind])

    else

      redirect_to new_place_of_interest_path
    end


  end

  def show
    @place_of_interest = PlaceOfInterest.find(params[:id])
  end

  def edit
    @place_of_interest = PlaceOfInterest.find(params[:id])
    @image_edit = @place_of_interest.photos
=begin

    if @image_edit.length == 0
      @image_edit = []
      for i in 0..3
        @image = Photo.new
        @image_edit.append(@image)
      end
      return
    end


    if @image_edit.length > 0 and @image_edit.length <= 3
      n =0
      case @image_edit.length
        when 1
          n = 2
        when 2
          n =1
        when 3
          n=0
      end
      for i in 0..n
        @image = Photo.new
        @image_edit.append(@image)
      end
    end
=end


  end

  def update
    @place_of_interest = PlaceOfInterest.find(params[:id])
    if @place_of_interest.update_attributes(place_of_interest_params)

      if params[:place_photo_0].present?
        @image0 = @place_of_interest.photos[0]
        if @image0.nil?
          @image0 = @place_of_interest.photos.create(photo_path: params[:place_photo_0],
                                                     place_of_interest_id: @place_of_interest.id);
          @image0.save
        else
          @image0.photo_path = params[:place_photo_0]
          @image0.save

        end

      end

      if params[:place_photo_1].present?
        @image1 = @place_of_interest.photos[1]
        if @image1.nil?
          @image1 = @place_of_interest.photos.create(photo_path: params[:place_photo_1],
                                                     place_of_interest_id: @place_of_interest.id);
          @image1.save
        else
          @image1.photo_path = params[:place_photo_1]
          @image1.save
        end

      end


      if params[:place_photo_2].present?
        @image2 = @place_of_interest.photos[2]
        if @image2.nil?
          @image2 = @place_of_interest.photos.create(photo_path: params[:place_photo_2],
                                                     place_of_interest_id: @place_of_interest.id);
          @image2.save
        else
          @image2.photo_path = params[:place_photo_2]
          @image2.save
        end

      end


      if params[:place_photo_3].present?
        @image3 = @place_of_interest.photos[3]
        if @image3.nil?
          @image3 = @place_of_interest.photos.create(photo_path: params[:place_photo_3],
                                                     place_of_interest_id: @place_of_interest.id);
          @image3.save
        else
          @image3.photo_path = params[:place_photo_3]
          @image3.save
        end

      end


      redirect_to place_of_interests_path(kind: @place_of_interest.kind)
    else
      redirect_to :action => 'edit'

    end


  end

  def delete
    @place_of_interest = PlaceOfInterest.find(params[:id])
  end

  def destroy
    @place_of_interest = PlaceOfInterest.find(params[:id])
    kind = @place_of_interest.kind
    @place_of_interest.destroy
    @place_of_interests = PlaceOfInterest.where('place_of_interests.kind = :kind_param', kind_param: kind).paginate(page: params[:page], per_page: 4)
  end

  def filter
    @place_of_interests = PlaceOfInterest.where("lower(name) like (?) AND kind = (?)", "%"+params[:place_to_search]+"%", params[:kind_of_place])
  end


  private

  def place_of_interest_params
    params.require(:place_of_interest).permit(:name, :description, :place_photo_0, :place_photo_1, :place_photo_2,:place_photo_3, :latitude, :longitude, :city, :country, :kind)

  end


end
