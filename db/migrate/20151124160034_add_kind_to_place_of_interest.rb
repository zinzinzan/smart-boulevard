class AddKindToPlaceOfInterest < ActiveRecord::Migration
  def change
    add_column :place_of_interests, :kind, :string
  end
end
