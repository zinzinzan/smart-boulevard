class PromotionsController < ApplicationController

  before_action :logged_in_user, only: [:index, :edit, :update, :destroy, :create]

#------------------------------------------------------------------
  def index
    if params[:per_page].present?
      Promotion.per_page = params[:per_page]
    end
    if current_user.admin
      @promotions = Promotion.all.paginate(page: params[:page])
    else
      @promotions = Promotion.where("store_id = ?", current_user.store).paginate(page: params[:page])
    end


  end

  def show
    @promo = Promotion.find(params[:id])
  end


  def new
    @promo = Promotion.new
  end

#------------------------------------------------------------------

  def create
    if current_user.admin
      @promo = Promotion.create(promo_params)

      if params[:initial_date].present?
        init_date = params[:initial_date]
        init_time = params[:initial_time]
        date_string = init_date + " " + init_time +" "+ "-0430"
        event_datetime = DateTime.strptime(date_string, '%m/%d/%Y %H:%M:%S %P %:z')
        @promo.initial_date = event_datetime

        final_date = params[:finish_date]
        final_time = params[:finish_time]
        final_string = final_date + " " + final_time +" "+ "-0430"
        final_datetime = DateTime.strptime(final_string, '%m/%d/%Y %H:%M:%S %P %:z')
        @promo.finish_date = final_datetime
      end

    else
      @promo = Promotion.create(promo_params)
      @promo.store = current_user.store

      if params[:initial_date].present?
        init_date = params[:initial_date]
        init_time = params[:initial_time]
        date_string = init_date + " " + init_time +" "+ "-0430"
        event_datetime = DateTime.strptime(date_string, '%m/%d/%Y %H:%M:%S %P %:z')
        @promo.initial_date = event_datetime

        final_date = params[:finish_date]
        final_time = params[:finish_time]
        final_string = final_date + " " + final_time +" "+ "-0430"
        final_datetime = DateTime.strptime(final_string, '%m/%d/%Y %H:%M:%S %P %:z')
        @promo.finish_date = final_datetime
      end

    end

    @promo.count_notification = 0
    if @promo.save

      if params[:image].present?
        @promo.image = params[:image]
        @promo.save
      end

      flash[:success] = "Promocion Creada!"
      redirect_to promotions_path
    else
      render 'new';
    end

#------------------------------------------------------------------

  end

  def edit
    @promo = Promotion.find(params[:id])
  end

#------------------------------------------------------------------

  def update
    @promo = Promotion.find(params[:id])
    if @promo.update_attributes(promo_params)
      if params[:image].present?
        @promo.image = params[:image]
        @promo.save
      end

      if params[:initial_date].present?
        init_date = params[:initial_date]
        init_time = params[:initial_time]
        date_string = init_date + " " + init_time +" "+ "-0430"
        event_datetime = DateTime.strptime(date_string, '%m/%d/%Y %H:%M:%S %P %:z')
        @promo.initial_date = event_datetime

        final_date = params[:finish_date]
        final_time = params[:finish_time]
        final_string = final_date + " " + final_time +" "+ "-0430"
        final_datetime = DateTime.strptime(final_string, '%m/%d/%Y %H:%M:%S %P %:z')
        @promo.finish_date = final_datetime
        @promo.save
      end

      flash[:success] = "Promocion actualizada"
      redirect_to promotions_path
    else
      render 'edit'
    end
  end

#------------------------------------------------------------------

  def destroy
    @promo = Promotion.find(params[:id]).destroy
    if current_user.admin
      @promotions = Promotion.all.paginate(page: params[:page], per_page: 4)
    else
      @promotions = Promotion.where("store_id = ?", current_user.store).paginate(page: params[:page], per_page: 4)
    end
    flash[:success] = "Promocion borrada"

  end

  def filter
    @promotions = Promotion.where("lower(title) like (?)", "%" + params[:starts_with] + "%");
  end

#------------------------------------------------------------------

  private

  def promo_params
    params.require(:promotion).permit(:title, :rate, :price, :notification_body,
                                      :promo_price, :conditions, :image, :store_id,
                                      :init_date, :finish_date)
  end
#------------------------------------------------------------------


end