class ApplicationMailer < ActionMailer::Base
  default from: 'smartboulevard122@gmail.com'
  layout 'mailer'
end
