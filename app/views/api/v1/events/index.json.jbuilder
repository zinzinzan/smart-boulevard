json.events @events do |event|
  json.id event.id
  json.name event.name
  json.date event.date
  json.projection event.projection
  json.city event.city
  json.image request.protocol + request.host_with_port + event.image.to_s
end