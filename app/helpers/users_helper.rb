module UsersHelper

  def gravatar_for(user, options = {size: 80})
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end

  #cant use constan form initilizer/constant.rb the helper have another namespace or scope

  def rol_translator role_name
    if role_name == 'Admin_user'
      return 'Administrador'
    end

    if role_name == 'App_user'
      return 'Usuario Móvil'
    end

    if role_name == 'Web_user'
      return 'Usuario Web'
    end

    if role_name == 'Social_network_user'
      return 'Usuario red Social'
    end
  end

end
