json.comments @comments do |comment|
  json.body comment.body
  json.author User.find(comment.user_id).user_name
  json.created_at time_ago_in_words(comment.created_at)
  json.created comment.created_at
end