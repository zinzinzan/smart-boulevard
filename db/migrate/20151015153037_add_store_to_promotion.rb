class AddStoreToPromotion < ActiveRecord::Migration
  def change
    add_reference :promotions, :store, index: true
    add_foreign_key :promotions, :stores
  end
end
