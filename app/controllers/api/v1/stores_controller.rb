class Api::V1::StoresController < Api::ApiController

  respond_to :json

  #---------------------------------------------------------------------

  def init
  end

  #----------------------------------------------------------------------


  def index
    @stores = Store.all;
  end

  #----------------------------------------------------------------------

  def show
    @store = Store.find(params[:id])
  end

  #----------------------------------------------------------------------

  def index_rest
    @stores = Store.where("kind = ? ", RESTAURANT);
  end

  #----------------------------------------------------------------------

  def index_kiosco
    @stores = Store.where("kind = ? ", KIOSCO);
  end


  def paginate_rest
    current_page = Integer(params[:pag], 10)
    per_page = 2
    records_fetch_point = (current_page - 1) * per_page
    @stores = Store.where('kind = ?', RESTAURANT).limit(per_page).offset(records_fetch_point)
  end

  def paginate_kiosco
    current_page = Integer(params[:pag], 10)
    per_page = 2
    records_fetch_point = (current_page - 1) * per_page
    @stores = Store.where('kind = ?', KIOSCO).limit(per_page).offset(records_fetch_point)
  end

end
