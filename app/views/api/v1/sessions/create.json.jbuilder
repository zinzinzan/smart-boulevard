json.success 1

json.user do
  json.username  @user.user_name
  json.email  @user.email
  json.api_key @user.api_key
end