json.events @projections do |projection|
  json.id projection.id
  json.name projection.name
  json.date projection.date
  json.projection projection.projection
  json.city projection.city
  json.description projection.detail
  json.place_of_interest_id projection.place_of_interest_id
  json.image request.protocol + request.host_with_port + projection.image.to_s
end