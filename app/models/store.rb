class Store < ActiveRecord::Base
  has_one :user, dependent: :destroy


  has_many :promotions, dependent: :destroy
  has_many :photos, dependent: :destroy
  mount_uploader :logo, PictureUploader
  validates :name, :description, presence: true
  self.per_page = 5

end
