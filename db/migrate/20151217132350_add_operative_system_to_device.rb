class AddOperativeSystemToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :operative_system, :string
  end
end
