require 'test_helper'

class UserTest < ActiveSupport::TestCase


  def setup
    @user = User.new(user_name: "Example User", email: "user@example.com",
                     password: "foobar", password_confirmation: "foobar")
  end

#------------------------------------------------------------

  test "should be valid" do
    assert @user.valid?
  end

#------------------------------------------------------------
  test "name should be preset" do
    @user.user_name = " "
    assert_not @user.valid?
  end

#------------------------------------------------------------
  test "user name should not be too long" do
    @user.user_name = "a"*51
    assert_not @user.valid?
  end

#------------------------------------------------------------

  test "email should be present" do
    @user.email = " ";
    assert_not @user.valid?
  end

#------------------------------------------------------------

  test "email should not be too long" do
    @user.email = "a"*244 + "@example.com";
    assert_not @user.valid?
  end

#------------------------------------------------------------

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_USER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

#------------------------------------------------------------

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com USERfoo.COM A_USERfoo.bar.org]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      assert_not @user.valid?, "#{invalid_address.inspect} should be valid"
    end
  end

#------------------------------------------------------------

  test "email addresses should be unique" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    assert_not duplicate_user.valid?
  end

#------------------------------------------------------------

  test "" do
    @user.password = @user.password_confirmation = "a"*5;
    assert_not @user.valid?
  end

#------------------------------------------------------------

  test "authenticated? should return false for a user with nil digest" do
    assert_not @user.authenticated?('')
  end

#------------------------------------------------------------


end
