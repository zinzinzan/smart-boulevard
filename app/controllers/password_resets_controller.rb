class PasswordResetsController < ApplicationController

  before_action :get_user,   only: [:edit, :update]
  before_action :valid_user, only: [:edit, :update]

  def edit

  end


  
  def update
    if params[:user][:password].empty?
      @user.errors.add(:password, 'No puede estar vacia')
      render 'edit'
    elsif @user.update_attributes(user_params)
      redirect_to successfull_password_changed_path
    else
      render 'edit'
    end
  end


  private

  def user_params
    params.require(:user).permit(:password, :password_confirmation)
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  # Confirms a valid user.
  def valid_user
    unless @user && @user.authenticated?( params[:id], :reset)
      redirect_to root_url
    end
  end
end