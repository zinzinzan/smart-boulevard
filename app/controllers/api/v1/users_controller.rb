class Api::V1::UsersController < Api::ApiController



  def create
    @role = Role.find_by(role_name: APP_USER)
    @user = User.new(user_params)
    @user.country = params[:country][:name]
    @user.role = @role

    if params[:province].present?
      @user.province = params[:province][:name]
    end
    if params[:city].present?
      @user.city = params[:city][:name]
    end

    begin
      @user.save!

      Device.create!(token_device: params[:registration_id],
                     operative_system: params[:os], user_id: @user.id)

    rescue ActiveRecord::RecordInvalid
      render json: {sucess: '0'}
      return
    end

    return

  end

  #------------------------------------------------------------------------------------------

  def update_user_device
    email = params[:email]
    token_device = params[:token_device]
    os = params[:os]
    begin

      @user = User.find_by_email(email)
      if @user

        device = Device.find_by_user_id(@user.id)
        if device
          device.update!(token_device: token_device, operative_system: os)
        else
          device = Device.new(user_id: @user.id, operative_system: os, token_device: token_device)
          @user.device = device;
          @user.save!
        end
      else
        render json: {error: 'usuario no encontrado'} and return
      end
    rescue Exception => e
      render json: {erro: e.message} and return
    end

    render json: {success: 'true'}

  end

  #------------------------------------------------------------------------------------------

  private

  def user_params
    params.permit(:user_name, :email, :last_name, :first_time, :password,
                  :password_confirmation, :country, :city, :province, :gender,
                  :born_date)
  end

end
