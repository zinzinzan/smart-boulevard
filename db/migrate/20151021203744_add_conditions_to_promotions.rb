class AddConditionsToPromotions < ActiveRecord::Migration
  def change
    add_column :promotions, :conditions, :string
  end
end
